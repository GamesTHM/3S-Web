/**
 * Choosing a faction, when playing the first time
 * @param args
 * @constructor
 */
var FactionChooser = function (args) {

    FullScreenElement.call(this, args);
    this.addClassName("FactionChooser");
    this.shown = false;


    this.isShown = function () {
        return this.shown;
    },

        this.show = function () {

            hideHud();
            hud.viewElements['actions'].show();
            hud.viewElements['title'].show();


            this.map = document.getElementById('map');


            this.factionChooser = document.createElement("div");
            this.factionChooser.id="factionChooser";
            this.factionChooser.classList.add("faction_chooser");

            this.factionImg= document.createElement("div");
            this.factionImg.id="factionImg";

            this.factionChooserTitle= document.createElement("p");
            this.factionChooserTitle.id="factionChooser_title";
            this.factionChooserTitle.classList.add("TextColorGradient");

            this.factionChooserContent= document.createElement("div");
            this.factionChooserContent.id="factionLore";
            this.factionChooserContent.classList.add("faction_content");



            this.factionApplyButton= document.createElement("div");
            this.factionApplyButton.id="setFaction";




            this.factionChooser.appendChild(this.factionImg);
            this.factionChooser.appendChild(this.factionChooserTitle);
            this.factionChooser.appendChild(this.factionChooserContent);
            this.factionChooser.appendChild(this.factionApplyButton);




            this.map.appendChild(this.factionChooser);

            this.map.style.backgroundColor = 'black';
            this.map.className = '';
            this.container = this.factionChooser;
            this.load('SCIENCE', 'PMD3wUzf7p');
            this.shown = true;
        }, this.close = function () {
        showHud();
        this.container.innerHTML = '';
        this.shown = false;
    },
        this.load = function (faction, factionID) {

            hud.viewElements.title.setTitle('FRAKTIONSWAHL');
            hud.viewElements.title.setSubTitle(faction);

            var Fac = Parse.Object.extend("Faction");
            var query = new Parse.Query(Fac);
            var that = this;
            query.include('name');
            query.include('headerimage');
            query.include('lore');
            query.equalTo('objectId', factionID);
            query.find({
                success: function (results) {
                    for (var i = 0; i < results.length; i++) {
                        var object = results[i];
                        var id = object.id;
                        fac = object;
                        faction_img = object.get('headerimage').get('image').url();
                        faction_lore = object.get('lore').get('content');
                        if (faction_img != null && faction_lore != null) {
                            that.factionImg.style.backgroundImage = "url('" + faction_img + "')";
                            that.factionChooserTitle.innerHTML=faction;
                            that.factionChooserContent.innerHTML = faction_lore;
                            that.factionApplyButton.style.backgroundImage = "url('./img/buttons/icon_check.png')";
                            that.factionApplyButton.onclick = function () {
                                that.setUserFaction(fac);
                            };
                            that.setBackgroundColorByFaction(document.getElementById('factionChooser'));
                        }
                    }
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });


        },


        this.setUserFaction = function (fac) {
            var Campus = Parse.Object.extend("Campus");
            campus = new Campus();
            campus.id = 'u5Wcb5IcaJ';

            user = Parse.User.current();
            user.set("Faction", fac);
            user.set('campus', campus);
            user.save(null,
                {
                    success: function (user) {
                        window.location.href = mainHTML; //"main.html";
                    },
                    error: function (user, error) {
                        console.log('Error : ' + error.message);
                    }
                });
        },

        this.setActionButtons = function () {
            hud.viewElements['title'].setTitle('FRAKTIONSWAHL');
            hud.viewElements['title'].setSubTitle('SCIENCE');

            actions = hud.viewElements['actions'];
            var that = this;
            actions.setActionButtonLeft(new ActionButton(function () {
              that.load('SECRET', 'YMW32WTOz7');
            }, './img/buttons/faction/icon_fractionselectsecret.png'));
            actions.setActionButtonMiddle(new ActionButton(function () {
                that.load('SCIENCE', 'PMD3wUzf7p');
            }, './img/buttons/faction/icon_fractionselectscience.png'),true);
            actions.setActionButtonRight(new ActionButton(function () {
              that.load('SOCIETY', 'QVVtUCv5Tm');
            }, './img/buttons/faction/icon_fractionselect.png'));

        },

        this.onFocusLeave = function () {

        };


};
