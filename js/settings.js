var Settings = function (args) {
  ViewElement.call(this, args);
  this.addClassName("Settings");

  this.settingsElements = [];
  this.container;
  this.map = document.getElementById("map");
  this.menuButton = document.getElementById("picture");

  this.show = function (args) {
    this.loadUserSettings(false);
    disableLocationList();
    getLanguageText("settings","title","System",true, hud.viewElements['title'].title_name);
    getLanguageText("settings","subtitle","Settings",true, hud.viewElements['title'].sub_title_name);

    this.fragment= document.createElement("div");
    this.fragment.id="fragment";

    this.fragmentContent = document.createElement("div");
    this.fragmentContent.id ="fragment_content";
    this.fragmentContent.classList.add("faqContainer");

    this.settingsTitle = document.createElement("p");
    this.settingsTitle.id ="settingsTitle";
    this.settingsTitle.classList.add("TextColorGradient");

    getLanguageText("settings","subtitle","Settings",true, this.settingsTitle);

    this.settingsContainer = document.createElement("div");
    this.settingsContainer.id ="settingsContainer";

    this.fragmentContent.appendChild(this.settingsTitle);
    this.fragmentContent.appendChild(this.settingsContainer);

    this.fragment.appendChild(this.fragmentContent);

    this.map.appendChild(this.fragment);

    this.container=this.fragmentContent;
    this.divAktuell = this.fragment;
    this.map.style.backgroundImage = "url('./img/kreuz.png')";
    this.map.className = '';

    this.setBackgroundColorByFaction(this.divAktuell);

    this.reload();
    this.shown = true;
    this.resize();

    this.fragment.classList.add('content_container');
    this.fragment.classList.add('animated');
    this.fragment.classList.add('slideInUp');
  },
  this.reload = function () {
    this.draw();
    this.load();
  },
  this.close = function () {
    enableLocationList();
    this.divAktuell.remove();
    //this.container.innerHTML = '';
    this.shown = false;
  },
  this.hide = function () {

  },
  this.setActionButtons = function () {
    actions = hud.viewElements['actions'];
    actions.setActionButtonLeft(
      new ActionButton(
        function () {
          showFragment('Profil', {'id': 'map'});
        },
        './img/buttons/Placeholder_profilpic.png'
      )
    );
    actions.setActionButtonMiddle(
      new ActionButton(
        function () {
          showFragment('Settings', {'id': 'map'});
        },
        './img/buttons/profil/icon_settings.png'
      ), true
    );
    actions.setActionButtonRight(ActionButton.CLOSE);
  },
  this.load = function () {
    this.loadUserSettings(true);
    this.loadCampusList();
  },
  this.save = function () {
    var selectedCampus = this.campusListItem[this.campusList.selectedIndex];

    var currentUser = Parse.User.current();
    var currentUserSettings = currentUser.get('settings');
    var campusChanged = false, settingChanged = false;
    var settingInputNode, settingValue;
    var promises = [];

    if(currentUser.get("campus").id != selectedCampus[0]) {
      currentUser.set('campus', selectedCampus[2]);
      campusChanged = true;
    }
    for (var key in currentUserSettings.attributes) {
      settingInputNode = document.getElementById(key);
      if(settingInputNode) {
        switch(settingInputNode.type) {
          case "checkbox":
            settingValue = settingInputNode.checked;
            break;
          case "number":
          case "text":
            settingValue = settingInputNode.value;
            break;
        }
        if(currentUserSettings.get(key) != settingValue) {
          currentUserSettings.set(key,settingValue);
          settingChanged = true;
        }
      }
    }

    if(campusChanged)
      promises.push(currentUser.save(null, {sessionToken: currentUser.getSessionToken()}));
    if(settingChanged)
      promises.push(currentUserSettings.save(null, {sessionToken: currentUser.getSessionToken()}))

    if(promises.length > 0) {
      Parse.Promise.when(promises)
      .then(
        function (results) {
          if(results[0].get("campus") != undefined)
            current_campus = results[0].get("campus");
          $.notify("Einstellungen gespeichert", 'success');
          loop();
        },
        function (user, error) {
          $.notify('Fehler beim Speichern der Einstellungen', 'error');
          console.log('Error : ' + error.message);
        }
      );
    }
  },
  this.loadCampusList = function () {
    var query = new Parse.Query("Campus");

    query.include('name');
    query.equalTo("visibility", true);

    query.find().then(
      function (results) {
        var campusId = Parse.User.current().get("campus").id;
        var selectedIndex;
        this.campusListItem = [];
        if (results.length == 0) {
          console.log("No campusses found");
          return;
        }
        for (var i = 0; i < results.length; i++) {
          var object = results[i];
          var id = object.id;
          var name = object.get('name').get('deu');
          this.campusListItem.push([id, name, object]);
          if(id == campusId) selectedIndex = i;
        }
        for (i = 0; i < this.campusListItem.length; i++) {
          opt = document.createElement('option');
          opt.value = this.campusListItem[i][0];
          opt.innerHTML = this.campusListItem[i][1];
          this.campusList.appendChild(opt);
        }
        if(selectedIndex != undefined)
          this.campusList.selectedIndex = selectedIndex;
      }.bind(this),
      function (error) {
        console.log("Error: " + error.code + " " + error.message);
      }
    );
    this.campusList = document.createElement('select');
    this.campusListLabel = document.createElement('p');

    this.campusList.className = 'setting';
    getLanguageText("settings","campus","Campus",true, this.campusListLabel);
    var currentElementContainer = document.createElement('div');
    currentElementContainer.id = 'setting' + this.settingsElements.length;
    currentElementContainer.classList.add('setting-option');
    currentElementContainer.appendChild(this.campusListLabel);
    currentElementContainer.appendChild(this.campusList);
    this.settingsContainer.appendChild(currentElementContainer);
  },
  this.draw = function () {
    var abgabeDiv = document.createElement('div');
    abgabeDiv.className = 'save_container';

    this.abgabe = document.createElement('img');
    this.abgabe.src = './img/buttons/icon_check.png';
    this.abgabe.className = 'Abgabe';

    abgabeDiv.appendChild(this.abgabe);
    this.settingsContainer.appendChild(abgabeDiv);

    this.abgabe.onclick = function () {
      this.save();
    }.bind(this);
  },
  this.resize = function (event) {
    if (isLandscapeMode()) this.divAktuell.className = 'fragmentLandscape';
    else this.divAktuell.className = ''
  },
  this.loadUserSettings = function (draw) {
    var currentUser = Parse.User.current();
    settings = this;
    if(currentUser == null) return;
    var userSettings = currentUser.get('settings');
    var defaultSettings = [
      {key: "useBrowserGPS", value: false},
      {key: "useDevVersion", value: false},
      {key: "useNotifications", value: false},
      {key: "language", value: "deu"},
    ];

    if (userSettings == null) {
      var UserSettings = Parse.Object.extend("UserSetting");
      userSettings = new UserSettings();
      for (var i = 0; i < defaultSettings.length; i++) {
        userSettings.set(defaultSettings[i].key,defaultSettings[i].value);
      }

      currentUser.set('settings', userSettings);
      currentUser.save(null, { sessionToken: currentUser.getSessionToken() })
      .then(
        function (user) {
          this.loadUserSettings(draw);
        }.bind(this),
        function (user, error) {
          console.log("Error while saving user settings: " + error.code + " " + error.message);
        }
      );
      return;
    }

    userSettings.fetch().then(
      function (settings) {
        var saveSettings = false;
        for (var i = 0; i < defaultSettings.length; i++) {
          if(settings.get(defaultSettings[i].key) == undefined) {
            settings.set(defaultSettings[i].key,defaultSettings[i].value);
            saveSettings = true;
          }
        }
        if(saveSettings) {
          settings.save().then(
            function (user) {
              this.loadUserSettings(draw);
            }.bind(this),
            function (user, error) {
              console.log("Error while saving user settings: " + error.code + " " + error.message);
            }
          );
          return;
        }
        this.buildSettingsFromUserSettings(userSettings, draw);
      }.bind(this),
      function (error) {
        console.log("Error while fetching user settings: " + error.code + " " + error.message);
      }
    );
  },
  this.buildSettingsFromUserSettings = function (userSettings, draw) {
    for (var key in userSettings.attributes) {
      var currentElementContainer = document.createElement('div');
      currentElementContainer.id = 'setting' + this.settingsElements.length;
      currentElementContainer.classList.add('setting-option');
      var currentElement;
      var currentElementText;
      var currentSettings = userSettings.get(key);
      switch (typeof currentSettings) {
        case 'boolean':
        currentElement = document.createElement('div');
        currentElement.className = 'booleanCheck setting';
        var input = document.createElement('input');
        input.checked = currentSettings;
        currentElementText = document.createElement('p');
        getLanguageText("settings",key,key,false,currentElementText);
        input.type = 'checkbox';
        input.id = key;
        // input.setAttribute('name', 'check');
        // input.onclick = function () {
        //   userSettings.set(this.id, this.checked);
        //   userSettings.save();
        // };
        var label = document.createElement('label');
        label.setAttribute('for', key);

        currentElement.appendChild(input);
        currentElement.appendChild(label);

        break;
        case 'number':
        currentElement = document.createElement('input');
        currentElement.value = currentSettings;
        currentElementText = document.createElement('p');
        getLanguageText("settings",key,key,false,currentElementText);
        currentElement.type = 'number';
        currentElement.id = key;
        // currentElement.onclick = function () {
        //   userSettings.set(this.id, this.value);
        //   userSettings.save();
        // };
        break;
        case 'string':
        currentElement = document.createElement('input');
        currentElement.value = currentSettings;
        currentElementText = document.createElement('p');
        getLanguageText("settings",key,key,false,currentElementText);
        currentElement.type = 'text';
        currentElement.id = key;
        currentElement.classList.add('setting');
        // currentElement.onclick = function () {
        //   userSettings.set(this.id, this.value);
        //   userSettings.save();
        // };
        break;

        default:
        case 'undefined':
        continue;
        break
      }

      this.settingsElements.push([key, currentElementContainer, currentSettings]);

      if (!currentElementText || !currentElementText || !currentElementContainer || !draw)
      continue;

      // currentElement.className = 'setting';
      currentElementContainer.appendChild(currentElementText);
      currentElementContainer.appendChild(currentElement);

      this.settingsContainer.appendChild(currentElementContainer);
    }
  },
  this.getUserSetting = function (key) {
    var currentUser = Parse.User.current();
    if (!currentUser) return null;
    if (this.settingsElements.length == 0) this.loadUserSettings(false);

    for (i = 0; i < this.settingsElements.length; i++) {
      if (this.settingsElements[i][0] == key)
      return this.settingsElements[i][2];
    }
    return null;
  }
};
