/**
 * Create a job
 * @param id
 * @param questID
 * @param job
 * @param jobFunctionName
 * @param functionParams
 * @param storyPages
 * @param container
 * @param name
 * @param points
 * @param energy
 * @param icon
 * @param desc
 */
var job =function(id, questID, job, jobFunctionName, functionParams, storyPages,
  container, name, points, energy, icon, desc, jobIsFinished, jobCheckIconUrl)
{
  this.id = id;
  this.questID = questID;
  this.job = job;
  this.jobFunctionName = jobFunctionName;
  this.functionParams = functionParams;
  this.parameters = [];
  this.container = container;
  this.storyPages = storyPages;

  this.name=name;
  this.points = points;
  this.energy = energy;
  this.icon = icon;

  this.desc = desc;

  this.jobIsFinished = jobIsFinished;
  this.jobCheckIconUrl = jobCheckIconUrl;

  if(functionParams) {
    var object, id, text, name, optional, type;
    for(var i = 0; i < functionParams.length; i++)
    {
      object = functionParams[i];
      id = object.id;
      text = object.get('LocalizedString').get('deu');
      name = object.get('name');
      optional = object.get('optional');
      type = object.get('type');

      this.parameters.push(new Parameter(id, text, name, optional, type, this));
    }
  } else {
    this.parameters.push(new Parameter());
  }
    /**
     * Finish a job
     * @returns {boolean}
     */
  this.finishJob = function() {

    if(this.storyPages != null) {
      var args = {};
      args['questDetail_id'] = this.questID;
      args['StoryPages'] = this.storyPages;
      args['activeJobId'] = this.id;
      args['jobCheckIconUrl'] = this.jobCheckIconUrl;
      showFragment('StoryDialog', args);
      return false;
    }
    else {
      //Parameter abarbeiten
      for (var i = 0; i < this.parameters.length; i++) {
          this.parameters[i].tryFinish(this);
      }
      return true;
    }
  },
  this.finish = function (paramFunction) {
    paramFunction['quest'] = this.questID;
    paramFunction['job'] = this.id;

    Parse.Cloud.run(this.jobFunctionName, paramFunction,
        {
          success: function (result) {
            if (result.success) {
              $.notify(result.message, 'success');
              updateHud();
              an = new Animation(document.getElementById("energy"));
              an.rubberband();
              an2 = new Animation(document.getElementById("point"));
              an2.rubberband();
              instance.close(true);
            }
            else {
              $.notify(result.message, "warning");
            }
            instance.finishAktiv = false;
          },
          error: function (error) {
            $.notify("Error: " + error.code + " " + error.message, "error");
            console.log(error);
            instance.finishAktiv = false;
          }
        });
  },
  this.create = function()
  {
    var container = document.getElementById('job_list');

    var job_container = document.createElement('tr');
    var job_icon_cell = document.createElement('td');
    var job_icon_container = document.createElement('div');
    var job_icon = document.createElement('img');
    var job_icon_check;// = document.createElement('img');
    var job_desc = document.createElement('td');
    var job_energy = document.createElement('td');
    var job_points = document.createElement('td');
    var job_stats = document.createElement('td');

    job_stats.className = 'stats_bottom';

    job_container.id = this.id;
    job_container.className = 'job_';
    job_desc.innerHTML = this.desc;
    job_desc.className = 'job_desc';

    job_icon.src = this.icon;
    job_icon.className = 'job_icon';
    job_icon_cell.appendChild(job_icon_container);
    job_icon_container.appendChild(job_icon);

    if(this.jobIsFinished) {
      job_icon_check = document.createElement('img');
      job_icon_check.className = 'job_icon';
      job_icon_check.src = this.jobCheckIconUrl;
      job_icon_container.appendChild(job_icon_check);
      job_desc.classList.add("success");
    }

    job_points.className = 'job_points';
    job_energy.className = 'job_energy';
    job_points.innerHTML ='<img class="questPointIcon" src="./img/ui/stats/punkte.png"/><br>' + this.points;
    job_energy.innerHTML ='<img class="questEnergyIcon" src="./img/ui/stats/energie.png"/><br>' + this.energy;

    job_container.appendChild(job_icon_cell);
    job_container.appendChild(job_desc);
    job_container.appendChild(job_energy);
    job_container.appendChild(job_points);
    // job_container.appendChild(job_stats);
    container.appendChild(job_container);
  }
};
