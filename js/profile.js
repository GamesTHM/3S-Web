var Profile = function (args) {
  ViewElement.call(this, args);
  this.addClassName("Profile");
  this.menuButton = document.getElementById("picture");

  this.show = function (args) {
    disableLocationList();
    hud.viewElements['title'].setTitle("System");
    hud.viewElements['title'].setSubTitle("Profil");

    this.profileFragment = document.createElement("div");
    this.profileFragment.id = "fragment";

    this.profileFragmentContent = document.createElement("div");
    this.profileFragmentContent.id = "fragment_content";

    this.imageContainer = document.createElement("div");
    this.imageContainer.id = "imageContainer";

    this.userImage = document.createElement("img");
    this.userImage.id = "userImage";

    this.imageContainer.appendChild(this.userImage);

    this.profileData = document.createElement("div");
    this.profileData.id = "profileData";

    this.profileTitle = document.createElement("p");
    this.profileTitle.id = "profileTitle";
    this.profileTitle.classList.add("TextColorGradient");
    this.profileTitle.innerHTML = "PROFIL";

    this.profileUserName = document.createElement("div");
    this.profileUserName.className = "profileDataEntry";

    this.profileLevel = document.createElement("div");
    this.profileLevel.className = "profileDataEntry";

    this.profileRank = document.createElement("div");
    this.profileRank.className = "profileDataEntry";

    this.profileData.appendChild(this.profileTitle);
    this.profileData.appendChild(this.profileUserName);
    this.profileData.appendChild(this.profileLevel);
    this.profileData.appendChild(this.profileRank);

    var logoutButton = document.createElement("button");
    logoutButton.innerHTML = "<img src='./img/buttons/profil/icon_logout.png'/> Ausloggen ";
    logoutButton.id="logoutButton";

    logoutButton.onclick = function () {
      Parse.User.logOut();
      window.location.href = indexHTML; //"index.html";
    };
    var div = document.createElement("div");
    div.style.width = "100%";
    div.style.float = "left";
    div.appendChild(logoutButton);

    this.header = document.createElement("div");
    this.header.id = "header";

    this.statsFooter = document.createElement("div");
    this.statsFooter.id = "statsFooter";

    this.header.appendChild(this.imageContainer);
    this.header.appendChild(this.profileData);
    this.header.appendChild(div);

    this.profileFragmentContent.appendChild(this.header);
    // this.profileFragmentContent.appendChild(div);
    // this.profileFragmentContent.appendChild(this.statsFooter);

    this.profileFragment.appendChild(this.profileFragmentContent);

    this.htmlElement.appendChild(this.profileFragment);

    this.fileChooser = new FileChooser(this.imageContainer);
    this.fileChooser.drawUpload();

    this.htmlElement.style.backgroundImage = "url('./img/kreuz.png')";
    this.htmlElement.className = '';
    this.container = this.profileFragmentContent;
    this.divAktuell = this.profileFragment;
    this.reload();
    this.shown = true;
    this.resize();
    this.setBackgroundColorByFaction(this.divAktuell);

    this.profileFragment.classList.add('content_container');
    this.profileFragment.classList.add('animated');
    this.profileFragment.classList.add('slideInUp');
  },
  this.reload = function () {
    var username = currentUser.get("username");

    this.updateImage();
    this.profileUserName.innerHTML = username.toUpperCase();
    this.profileLevel.innerHTML = "LEVEL " + hud.viewElements.playerLevel.getLevel();
    this.calcUserRank();

    var data = {
      'Abgeschlossene Quests': 13,
      'Eingenommene Locations': 5
    };

    for (var key in data) {
      var p = document.createElement("p");

      p.classList.add("little");
      p.innerHTML = key + ": " + data[key];

      this.statsFooter.appendChild(p);
    }
    // todo get Data from parse

  },
  this.close = function () {
    enableLocationList();
    this.divAktuell.remove();
    this.shown = false;
  },
  this.hide = function () {

  },
  this.setActionButtons = function () {
    actions = hud.viewElements['actions'];
    actions.setActionButtonLeft(
      new ActionButton(
        function () {
          showFragment('Profil', {'id': 'map'});
        },
        './img/buttons/Placeholder_profilpic.png'
      ), true //set this as the active action button
    );
    actions.setActionButtonMiddle(
      new ActionButton(
        function () {
          showFragment('Settings', {'id': 'map'});
        },
        './img/buttons/profil/icon_settings.png'
      )
    );
    actions.setActionButtonRight(ActionButton.CLOSE);
  },
  this.resize = function (event) {
    if (isLandscapeMode()) this.divAktuell.className = 'fragmentLandscape';
    else this.divAktuell.className = ''
  },
  this.updateImage = function () {
    if (hud.viewElements.user.hasProfilePicture()) {
      this.userImage.style.backgroundImage = hud.viewElements.user.getUserImageData();
    } else {
      this.userImage.src = hud.viewElements.user.getProfilePictureLink();
    }
  },
  this.calcUserRank = function() {
    var userRankQuery = new Parse.Query("User");
    var currentUser = Parse.User.current();

    if(currentUser.get("points") == undefined) return;

    userRankQuery.greaterThan("points",currentUser.get("points"));
    userRankQuery.equalTo("campus",currentUser.get("campus"));
    userRankQuery.count().then(
      function(count) {
        this.profileRank.innerHTML = "Rang " + (count+1);
      }.bind(this),
      function(eror) {
        console.log("Highscore Rang konnte nicht geladen werden.");
      }
    );
    // this.profileRank.innerHTML = "Rang 100";
  }
};
