var faction_img;
var faction_shown;

/**
 * Load faction with specific id
 * @param id Faction id
 */
function loadFaction(id)
{
  var Fac = Parse.Object.extend("Faction");
  var query = new Parse.Query(Fac);
  query.equalTo("objectId", id);
  query.find({
    success: function(results) {
      for (var i = 0; i < results.length; i++) {
        var object = results[i];
        var id = object.id;
        faction_img = object.get('logo');
        if(faction_img != null)
        {
          console.log('url: ' +faction_img.url());
          document.getElementById('faction').style.backgroundImage = "url('"+faction_img.url()+"')";
        }
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

/**
 * Load the factions logo
 * @param id Faction id
 */
function getFactionLogo(id)
{
  if(faction_img == null)
  {
    loadFaction(id);
  }
}

/**
 * Load the factions color
 * @param id Faction id
 */
function getFactionColor(id)
{
  var Fac = Parse.Object.extend("Faction");
  var query = new Parse.Query(Fac);
  query.equalTo("objectId", id);
  query.find({
    success: function(results) {
      for (var i = 0; i < results.length; i++) {
        var object = results[i];
        var id = object.id;
        current_faction_color = object.get('mainColor');
        updateHud();
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}
/**
 * Showing something about the faction
 */
function faction_show()
{
  var map = document.getElementById('map');
  map.innerHTML='<div id="fragment"></div>';
  map.style.backgroundImage = "url('./img/kreuz.png')";
  map.className = '';
  faction_load();
  faction_shown = true;
}
/**
 * Closing the Faction?
 */
function faction_close()
{
  var container = document.getElementById('fragment');
  container.innerHTML = '';
  faction_shown = false;
}
/**
 * Setting the factions content
 * @param args
 * @constructor
 */
var Faction =function(args){
    ViewElement.call(this,args);
    this.addClassName("Faction");

    this.show=function(args){
        hud.viewElements['title'].setTitle("System");
        hud.viewElements['title'].setSubTitle("Faction");
        this.htmlElement = document.getElementById('map');
        this.htmlElement.innerHTML='<div id="fragment"><div id="fragment_content"></div></div>';;
        this.htmlElement.style.backgroundImage = "url('./img/kreuz.png')";
        this.htmlElement.className = '';
        this.shown=true;
        this.load();
        this.frag = document.getElementById('fragment');
        this.setBackgroundColorByFaction(this.frag);
    }, this.load=function(){
        var container = document.getElementById('fragment_content')
        getWebElementFromCloud('faction',container);
    },this.close=function(args){
        var container = document.getElementById('fragment_content');
        container.innerHTML = '';
        faction_shown = false;
    },this.getColor=function(id){

        var Fac = Parse.Object.extend("Faction");
        var query = new Parse.Query(Fac);
        query.equalTo("objectId", id);
        query.find({
            success: function(results) {
                for (var i = 0; i < results.length; i++) {
                    var object = results[i];
                    var id = object.id;
                    current_faction_color = object.get('mainColor');
                    updateHud();
                }
            },
            error: function(error) {
                alert("Error: " + error.code + " " + error.message);
            }
        });
    };

    /**
     * Load the Faction
     * @param id
     */
    function load(id){
        var Fac = Parse.Object.extend("Faction");
        var query = new Parse.Query(Fac);
        query.equalTo("objectId", id);
        query.find({
            success: function(results) {
                for (var i = 0; i < results.length; i++) {
                    var object = results[i];
                    var id = object.id;
                    faction_img = object.get('logo');
                    if(faction_img != null)
                    {
                        console.log('url: ' +faction_img.url());
                        document.getElementById('faction').style.backgroundImage = "url('"+faction_img.url()+"')";
                    }
                }
            },
            error: function(error) {
                alert("Error: " + error.code + " " + error.message);
            }
        });
    }




};
function faction_load()
{
  var container = document.getElementById('fragment')
  getWebElementFromCloud('faction',container);
}
