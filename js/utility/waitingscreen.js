/**
 * Does some real shit. ???
 * @param refresh
 */
function loaded(refresh) {
    Parse.serverURL = serverUrlParse;
    Parse.initialize(appID, jsKey);

    //Setzen der Events !!!!--------------------------------------
    document.body.onresize = resizeHud();
    document.body.oncontextmenu = function() {return false;};
    $(window).on("orientationchange",function(event){ resizeHud(); if(instance != null) instance.resize(event); });
    //------------------------------------------------------------

    var currentUser = Parse.User.current();

    if (
      currentUser != null
      // && (window.location.pathname.indexOf('index.html')>-1)
      && (window.location.pathname.indexOf(indexHTML)>-1)
    ) {
      window.location.href = mainHTML; //"main.html";
    }

    if (
      currentUser != null
      // && !(window.location.pathname.indexOf('index.html')>-1)
      // && !(window.location.pathname.indexOf('register.html')>-1)
      && !(window.location.pathname.indexOf(indexHTML)>-1)
      && !(window.location.pathname.indexOf(registerHTML)>-1)
    ) {
      var user = hud.viewElements.user;

      currentUser.fetch().then(function(updatedUser) {
        var currentUser = updatedUser;

        current_campus = currentUser.get("campus");
        current_faction = currentUser.get("Faction");
        if (current_faction != null) {
          if (current_faction.id != null) {
            getFactionLogo(current_faction.id); //global function, defined in js/faction.js
            getFactionColor(current_faction.id); //global function, defined in js/faction.js
          }
        }
        if (current_faction == null) {
          showFragment('FactionChooser'); //global function, defined in js/utility/showFrag.js
        }
        else {
          showFragment('Map');
          clearLocations();
          updateLocations(); //global function, defined in js/location.js
          startClock(); //global function, defined in js/loop.js
        }

        user.setUserName(currentUser.get("username"),currentUser.get("level"));
        user.userElements.stats.setPoints(currentUser.get("points"));
        user.userElements.stats.setEnergy(currentUser.get("energy"));
      });

      hud.viewElements.locations.locations.location_button.htmlElement.onclick = function () {
          loop(); //global function, defined in js/loop.js
          if(settings.getUserSetting('useBrowserGPS')) {getLocationFromBrowser(focusUserLocation);}
          updateHud(); //global function, defined in js/utility/utility.js
      };
      user.userElements.picture.htmlElement.onclick = function () {
          showFragment('Profil', {'id': 'map'});
      };
      hud.viewElements.faction.htmlElement.onclick = function () {
          showFragment('News');
      };
    }
    else {
        if (
          // !(window.location.pathname.indexOf('index.html')>-1) //url doesn't point to index.thml
          // && !(window.location.pathname.indexOf('register.html')>-1)  // and not to register.html
          !(window.location.pathname.indexOf(indexHTML)>-1) //url doesn't point to index.thml
          && !(window.location.pathname.indexOf(registerHTML)>-1)  // and not to register.html
          || refresh // or page was refreshed
        ) {
            window.location.href = indexHTML; //"index.html";
        }
    }
    //Set Global Focus Listener
    document.body.onclick = function (e) {
        e = e || window.event;
        var element = (e.target || e.srcElement);

        // call Focus Listener
        if (instance != null)
            instance.onFocusLeave(element);
        if (overlayInstance != null)
            overlayInstance.onFocusLeave(element);
        // ...
    }
    $(".loader").fadeOut("slow");
}
