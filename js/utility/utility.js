var hudColorSet = false;
var hud;

addLoadEvent(function() {
  hud = new Hud();
  loaded(false);
});

// Opera 8.0+
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
// Firefox 1.0+
var isFirefox = typeof InstallTrigger !== 'undefined';
// At least Safari 3+: "[object HTMLElementConstructor]"
var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
// Internet Explorer 6-11
var isIE = /*@cc_on!@*/false || !!document.documentMode;
// Edge 20+
var isEdge = !isIE && !!window.StyleMedia;
// Chrome 1+
var isChrome = !!window.chrome && !!window.chrome.webstore;
// Blink engine detection
var isBlink = (isChrome || isOpera) && !!window.CSS;







/**
 * Gets a webelement from ParseCloud.
 * @param ObjectId
 * @param container
 */
function getWebElementFromCloud(ObjectId, container) {
    if (!ObjectId)
        return;
    var params = {'webElementId': ObjectId};
    getWebElementFromCloudWithParams(ObjectId, container, params);

}

/**
 * Gets a webelement from ParseCloud with extra parameters
 * @param ObjectId
 * @param container
 * @param params
 */
function getWebElementFromCloudWithParams(ObjectId, container, params) {
    if (!ObjectId && !params)
        return;

    Parse.Cloud.run('getWebElement', params, {
        success: function (WebElementDataHTML) {
            //if(!WebElementDataHTML)
            container.innerHTML = WebElementDataHTML;
        },
        error: function (error) {
            $.notify("Error: " + error.code + " " + error.message, "error");
            console.log("Error: " + error.code + " " + error.message, "error");
        }
    });
}

/**
 * Get a specific GUI graphic.
 * @param name
 * @param container
 */
function getGuiGraphic(name, container) {
    var param = Parse.Object.extend('GUIGraphic');
    var query = new Parse.Query(param);
    query.equalTo('graphicName', name);
    var that = this;
    query.find({
        success: function (results) {
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                var image = object.get("graphic");
                container.style.backgroundImage = "url('" + image.url() + "')";
            }
        },
        error: function (error) {
            $.notify("Error: " + error.code + " " + error.message, "error");
        }
    });
}
function loadGuiGraphics() {
    getGuiGraphic('title', document.getElementById('title'));
    getGuiGraphic('actions', document.getElementById('actions'));
    getGuiGraphic('stats', document.getElementById('stats'));
}

/**
 * Update the HUD. Refreshes levels and madness bar.
 */
function updateHud() {
    //loadGuiGraphics();

    currentUser = Parse.User.current();
    currentUser.fetch().then(function (updatedUser) {
        var currentUser = updatedUser;

        current_campus = currentUser.get("campus");
        current_faction = currentUser.get("Faction");

        getFactionLogo(current_faction.id);

        stats = hud.viewElements.user.userElements.stats;
        stats.setPoints(currentUser.get("points"));
        stats.setEnergy(currentUser.get("energy"));
        stats.setPoints(currentUser.get("points"));

        madnessLv = hud.viewElements['madnessLevel'];
        madnessLv.refresh(currentUser);

        playerLv = hud.viewElements['playerLevel'];
        playerLv.refresh(currentUser);

        hud.viewElements.user.setUserName(currentUser.get("username"),currentUser.get("level"));
        hud.viewElements['user'].setPicture(hud.viewElements.user.getProfilePictureLink());

        resizeHud();
        hud.setColor(current_faction_color, hud);
        setNotifyStyle();

        //$.notify.defaults({globalPosition: 'top center'});
        redraw();

    }.bind(this));
}

/**
 * Set the style of a notify bar.
 */
function setNotifyStyle() {
    $.notify.defaults({globalPosition: 'top center', style: '3s', autoHide: true});
    $.notify.addStyle('3s', {
        html: "<div>" +
        "<div class='clearfix'>" +
        "<div class='title' data-notify-text/>" +
        "</div>" +
        "</div>"
    });

    //listen for click events from this style
    $(document).on('click', '.notifyjs-3s-base .no', function () {
        //programmatically trigger propogating hide event
        $(this).trigger('notify-hide');
    });
    $(document).on('click', '.notifyjs-3s-base .yes', function () {
        //show button text
        alert($(this).text() + " clicked!");
        //hide notification
        $(this).trigger('notify-hide');
    });
}

/**
 * set the color for the HUD
 */
function setHudColor() {
    //if (hudColorSet == true) return;
    if (current_faction_color) {
        var colorDivs = ['title', 'actions', 'stats', 'map_btn', 'quest_btn', 'score_btn', 'location_button'];
        for (i = 0; i < colorDivs.length; i++) {

            img = document.getElementById(colorDivs[i]);
            paintBrush.destroyStash(img, true);
            setColorAttr(img);
            hudColorSet = true;
        }
        paintBrush.processFilters();
    }
}

/**
 * set the color attribute
 * @param id
 */
function setColorAttr(id) {
    if (id.id == 'map') return;
    if (id.id == 'main') return;
    if (!$(id).hasClass('filter-tint')) {
        var class_before = $(id).attr('class');
        if (class_before)
            $(id).attr('class', 'filter-tint ' + class_before);
        else
            $(id).attr('class', 'filter-tint');
    }
    $(id).attr('data-pb-tint-colour', current_faction_color);
    $(id).attr('data-pb-tint-opacity', '0.2');

}

/**
 * obvious
 */
function hideHud() {
    console.log('Hide Hud');
    hud.hide();
}

/**
 * obvious
 */
function showHud() {
    console.log('Show Hud');
    hud.show();
}
/**
 * make it even obvious
 */
function resizeHud() {
    hud.resize();
}
/**
 * Redraws something
 */
function redraw() {
    console.log('redraw');
    $('#main').hide();
    $('#main').get(0).offsetHeight; // no need to store this anywhere, the reference is enough
    $('#main').show();
}
HTMLElement.prototype.alpha = function (a) {
    current_color = getComputedStyle(this).getPropertyValue("background-color");
    match = /rgba?\((\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(,\s*\d+[\.\d+]*)*\)/g.exec(current_color);
    a = a > 1 ? (a / 100) : a;
    this.style.backgroundColor = "rgba(" + [match[1], match[2], match[3], a].join(',') + ")";
};
/**
 * Checks if were in landscpae mode.
 */
function isLandscapeMode() {
    return (window.innerHeight < window.innerWidth);
}

function checkHit(o1, o2) {
    var top_1 = o1.offsetTop;
    var top_2 = o2.offsetTop;
    var left_1 = o1.offsetLeft;
    var left_2 = o2.offsetLeft;
    var bottom_1 = top_1 + o1.offsetHeight;
    var bottom_2 = top_2 + o2.offsetHeight;
    var right_1 = left_1 + o1.offsetWidth;
    var right_2 = left_2 + o2.offsetWidth;

    return ( bottom_2 > top_1 && top_2 < bottom_1 )
        &&
        ( right_2 > left_1 && left_2 < right_1 )
        ;
}
function croppingImg(src, container, type, setterCallback) {

    var crop = document.createElement('div');
    crop.id = "crop";
    container.appendChild(crop);
    getDataUri(src, function (dataUri) {
        var basic = $('#crop').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: type
            },
            showZoomer: false,
        });
        basic.croppie('bind', {
            url: dataUri
        });
        setTimeout(function () {
            basic.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {

                setterCallback({
                    src: resp
                });
                container.removeChild(crop);
            });
        }, 500);
    });

}
/**
 * Gettin the data URI of an URL?
 * @param url
 * @param callback
 */
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous');

    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
        canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

        canvas.getContext('2d').drawImage(this, 0, 0);

        // Get raw image data
        //callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

        // ... or get as Data URI
        callback(canvas.toDataURL('image/png'));
    };

    image.src = url;
}
/**
 * Set a new user image
 * @param result
 */
function setUserImg(result) {
    var html;
    if (result.html) {
        html = result.html;
    }
    if (result.src) {
        $('#picture').css('background-image', 'url(' + result.src + ')');
    }
}
/**
 * Replace umlaut without an upper case like the ß
 * @param string
 * @returns {void|XML}
 */
function replaceAllUmlautsWithoutUpperCase(string) {
    if (string == null || string == '')
        return '';
    else
        return string.replace('&szlig;', 'ss');
}
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}
function getLanguageText(className, tag, def, uppercase, container) {
    var GUIString = Parse.Object.extend('GUIString');
    var query = new Parse.Query(GUIString);

    if(uppercase)
        def = def.toUpperCase();

    query.equalTo('classTag', className);
    query.equalTo('tag', tag);
    query.include('value');
    var that = this;
    query.find({
        success: function (results) {
            if(results.length == 0) {
                container.innerHTML = def;
                return;
            }
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                var value = object.get('value');
                if(value) {
                    lang = settings.getUserSetting('language');
                    if(lang == null)
                        lang = navigator.language.substr(0,2);
                    if (value.get(lang)) {
                        if (uppercase)
                            container.innerHTML = value.get(lang).toUpperCase();
                        else
                            container.innerHTML = value.get(lang);
                        return;
                    }
                }
                container.innerHTML = def;
            }
        },
        error: function (error) {
            $.notify("Error: " + error.code + " " + error.message, "error");
            container.innerHTML = def;
        }
    });
}
function getPicture(className, tag, def) {
    return def;
}
function distance(lon1, lat1, lon2, lat2) {
    var R = 6371; // Radius of the earth in km
    var dLat = (lat2-lat1).toRad();  // Javascript functions in radians
    var dLon = (lon2-lon1).toRad();
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    d = d*1000 //Distance in Metern
    return d;
}
if (typeof(Number.prototype.toRad) === "undefined") {
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }
}
