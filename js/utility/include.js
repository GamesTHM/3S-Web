head.load(
    "css/reset.css",
    "http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css",
    "css/styles.css",
    "css/job.css",
    "css/fragments/viewElements.css",
    "css/fragments/fragment.css",
    "css/fragments/map.css",
    "css/fragments/location_info.css",
    "css/fragments/story_dialog.css",
    "css/fragments/quest_list.css",
    "css/fragments/quest.css",
    "css/fragments/highscore.css",
    "css/fragments/location_detail.css",
    "css/fragments/faction_chooser.css",
    "css/notification.css",
    "css/colorFilter.css",
    "css/hud/stats.css",
    "css/hud/hud.css",
    "css/hud/menue.css",
    "css/hud/actionbar.css",
    "css/hud/titlebar.css",
    "css/hud/location_hud.css",
    "css/3rd/croppie.css",
    "css/overlay/overlay.css",
    "css/overlay/inputDialog.css",
    "css/fragments/profile.css",
    "css/fragments/faq.css",
    "css/fragments/settings.css",
    "css/fragments/news.css",
    "css/3rd/animate.css",
    //3rd part
    "js/3rd/jquery.min.js",
    "js/3rd/jquery-ui.js",
    "js/3rd/leaflet.js",
    "https://npmcdn.com/parse/dist/parse.min.js",
    "js/3rd/Chart.min.js",
    "js/3rd/common.js",
    "js/3rd/paintbrush.js",
    "js/3rd/notify.min.js",
    "js/3rd/croppie.min.js",
    "js/3rd/progressbar.js",
    //fragments
    "js/file_chooser.js",
    "js/viewElements/view_element.js",
    "js/viewElements/fullscreen_element.js",
    "js/viewElements/colorable.js",
    "js/viewElements/sub_title.js",
    "js/viewElements/action_button.js",
    "js/viewElements/action_element.js",
    "js/viewElements/title_element.js",
    "js/viewElements/hud.js",
    "js/viewElements/highscore_element.js",
    "js/viewElements/fullscreen_animated.js",
//     "js/viewElements/action_element.js",
//     "js/viewElements/faction_highscore_element.js",
    "js/viewElements/menu.js",
    "js/viewElements/stats.js",
    "js/viewElements/user.js",
    "js/viewElements/madness_level.js",
    "js/viewElements/player_level.js",

    "js/viewElements/faction_button.js",
    "js/viewElements/story_dialog.js",
    "js/viewElements/menu_button.js",
    "js/quest_element.js",

    "js/map.js",
    "js/quest.js",
    "js/quest_detail.js",
    "js/settings.js",
    "js/story.js",
    "js/global_vars.js",
    "js/highscore.js",
    "js/location.js",
    "js/locations.js",
    "js/location_info.js",
    "js/location_detail.js",
    "js/loop.js",
    "js/settings.js",
    "js/faction.js",
    "js/news.js",
    "js/profile.js",
    "js/job.js",
    "js/faction_chooser.js",
//     "js/faction_higthscore.js",
    "js/parameter.js",
    "js/loginSignUp.js",
    "js/faq.js",

    //utilitys
    "js/utility/utility.js",
    "js/utility/waitingscreen.js",
    "js/utility/showFrag.js",
    "js/utility/animation.js",

    //overlays
    "js/overlays/overlay.js",
    "js/overlays/inputDialog.js",
    "js/overlays/location_list_overlay.js"
    // function() {
    //   loaded(false);
    // }
);
// head.ready("ws",function() {
//   loaded(false);
// });
