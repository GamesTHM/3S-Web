var instance;
var instance_tag;
var overlayInstance;
function closeFragment(args) {
    if (instance && instance.isShown()) {
        instance.close(args);
        instance.menuButton && instance.menuButton.classList.remove("active_btn");
        instance = null;
    }
}
function closeOverlay(args) {
    if (overlayInstance && overlayInstance.isShown()) {
        overlayInstance.close(args);
        instance.refresh();
        instance.setActionButtons();
        overlayInstance = null;
    }
}
/**
 * Function sets the new choosen overlay
 * @param OverlayTag Tag which overlay to show
 * @param args something
 */
function showOverlay(OverlayTag, args, dialog) {
    console.log('Open Overlay: ' + OverlayTag);
    closeOverlay();
    switch (OverlayTag) {
        case "location_list":
            overlayInstance = new LocationListOverlay(args);
            break;
        case "LocationInfo":
            overlayInstance = new LocationInfo(args);
            break;
        case "InputDialog":
            overlayInstance = new InputDialog(args);
            break;
    }
    if (dialog != null && dialog == true) {
        overlayInstance.showDialog(args);
    }
    else
        overlayInstance.show(args);

    overlayInstance.setActionButtons();
}
/**
 * Function sets the new choosen fragment
 * @param FragmentTag Tag which Fragment to show
 * @param args something
 */
function showFragment(FragmentTag, args) {
    console.log('Open Fragment: ' + FragmentTag);
    closeOverlay();
    if(instance_tag == FragmentTag)
        return;
    closeFragment();
    switch (FragmentTag) {
        case "Map":
            instance = new Map(args);
            break;

        case "Quest":
            instance = new Quest(args);
            break;

        case "QuestDetail":
            instance = new QuestDetail(args);
            break;

        case "Highscore":
            instance = new Highscore(args);
            break;

        case "FactionHighscore":
            instance = new FactionHighscore(args);
            break;

        case "Location":
            instance = new LocationDetail(args);
            break;

        case "LocationInfo":
            instance = new LocationInfo(args);
            break;

        case "Profil":
            instance = new Profile(args);
            break;

        case "News":
            instance = new News(args);
            break;

        case "Settings":
            instance = new Settings(args);
            break;

        case "Faction":
            instance = new Faction(args);
            break;

        case "Faq":
            instance = new Faq(args);
            break;

        case "Story":
            instance = new Story(args);
            break;

        case "FactionChooser":
            instance = new FactionChooser(args);
            break;
        
        case "StoryDialog":
            instance = new StoryDialog(args);
            break;

        default:
            console.log('NO Fragment: ' + FragmentTag);
            return;
    }
    instance_tag = FragmentTag;
    instance.show(args);
    instance.menuButton && instance.menuButton.classList.add("active_btn");
    instance.setActionButtons();
    //changeActionButtons(FragmentTag);
}
/**
 * Log the Tag
 * @param Tag some tag
 */
function actionButtonsOnClick(Tag) {
    console.log('Clicked : ' + Tag);
}
