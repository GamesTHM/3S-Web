/**
 * Created by danielhahn on 28.07.16.
 */
var Animation = function (obj) {
    this.classes = new Array("Animation");
    this.htmlElement = obj;

    this.pulse = function (maxPulse) {
        var that = this;
        if(that.htmlElement == null)
            return;
        $(that.htmlElement).animate({
            width: 55, height: 65,
            opacity: 0.75
        }, 700, function() {
            $(that.htmlElement).animate({
                width: 45, height: 55,
                opacity: 1
            }, 700, function() {
                if(maxPulse >= 0)
                    that.pulse(--maxPulse);
            });
        });
    };

    this.bounce = function () {
        var that = this;
        if(that.htmlElement == null)
            return;
        $(that.htmlElement).addClass('animated bounce');
    };

    this.flip = function () {
        var that = this;
        if(that.htmlElement == null)
            return;
        $(that.htmlElement).addClass('animated flip');
    };

    this.rubberband = function () {
        var that = this;
        if(that.htmlElement == null)
            return;
        $(that.htmlElement).addClass('animated rubberBand');
    };

    this.glow = function(maxGlowTime, maxGlow) {
        var that = this;
        $(that.htmlElement).fadeOut(maxGlowTime, function(){
            $(that.htmlElement).fadeIn(maxGlowTime,function() {
                if(maxGlow >= 0)
                    that.glow(--maxGlow);
            });
        });
    };
    this.showAktive = function () {
        
    }
}