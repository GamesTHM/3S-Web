/**
 * Create the highscore view
 * @param args
 * @constructor
 */
var Highscore = function(args){
  ViewElement.call(this,args);
  this.addClassName("Highscore");

  this.container = document.getElementById('map');
  this.menuButton = document.getElementById("score_btn");
  this.shown=false;
  this.Elements = [];
  this.HighscoreElements={};

  this.show=function(args) {
    disableLocationList();
    hud.viewElements['title'].setTitle("Highscore");
    this.container.style.backgroundImage = "url('./img/kreuz.png')";
    this.container.className = '';

    this.load("user highscore");
    this.shown = true;
  },
  this.close=function(args) {
    enableLocationList();
    this.container.innerHTML = '';
    this.shown = false;
  },
  this.load = function(what) {
    if(what == "user highscore") {
      this.limit = 10;
      this.skip =  0;
      this.max = 100;

      hud.viewElements['title'].setSubTitle("Spieler");
      this.container.innerHTML=
        '<div id="score_list">' +
          '<div id="loadBtnDiv">' +
            '<label id="loadingLabel" class="centered-lbl">Die Highscore wird geladen...</label>' +
          '</div>' +
        '</div>';
      document.getElementById('score_list').onscroll = function(e) {
        if(
          e.target.scrollTop >=
          (e.target.scrollHeight - e.target.offsetHeight - 100)
          && !this.loading
          && this.skip < this.max
        ) {
          this.loading = true;
          this.loadUserHighscore();
        }
      }.bind(this);
      this.label = document.getElementById('loadingLabel');
      this.loadUserHighscore();
    } else if (what == "faction highscore") {
      hud.viewElements['title'].setSubTitle("Fraktionen");
      this.container.innerHTML = '<canvas id="myChart"></canvas>';
      this.loadFactionHighscore();
    }
  },
  this.loadUserHighscore = function(mode) {

    if(this.label.style.display == "none") {
      this.label.style.display = "block";
      this.label.innerHTML = "Lädt...";
    }

    var query = new Parse.Query("User");

    query.include('Faction')
    .equalTo("campus", current_campus)
    .descending("points")
    .limit(this.limit)
    .skip(this.skip)
    .find().then(
      this.onUserHighscoreLoaded.bind(this),
      this.onQueryError
    );

    this.skip += this.limit;
  },
  this.loadFactionHighscore = function() {
    var query = new Parse.Query("Faction");
    query.include('name');
    query.find().then(
      this.onFactionHighscoreLoaded,
      this.onQueryError
    );
  },
  this.onUserHighscoreLoaded = function(results) {
    if(this.label.style.display != "none") {
      this.label.style.display = "none";
    }

    for (var i = 0; i < results.length; i++) {
      var object = results[i];
      var id = object.id;
      var name = object.get('username');
      var point = Math.floor(object.get('points'));
      var faction = object.get('Faction');
      var img = object.get('image');
      var level = Math.floor(object.get('level'));
      var rank = (i+1+this.skip-this.limit);

      if(level == null || isNaN(level))
          level = 0;

      if(point == null || isNaN(point))
          point = 0;

      this.create(name, point, faction, img, id, this, level, rank);
    }
    this.setColor(current_faction_color,this);
    this.loading = false;
  },
  this.onFactionHighscoreLoaded = function(results) {
    var factionPoints = [];
    var color = [];
    var name = [];
    var data = {};
    data.datasets = [];

    for (var i = 0; i < results.length; i++) {
        var object = results[i];
        factionPoints[i] = object.get('points');
        name[i] = object.get('name').get('deu');
        color[i] = object.get('mainColor');
        colorLight = object.get('mainColor');
    }
    data.datasets =
      [{
          data: factionPoints,
          backgroundColor: color
          /* highlight: colorLight, */
      }];

    data.labels = name;

    Chart.pluginService.register({
      beforeRender: function (chart) {
          if (chart.config.options.showAllTooltips) {
              // create an array of tooltips
              // we can't use the chart tooltip because there is only one tooltip per chart
              chart.pluginTooltips = [];
              chart.config.data.datasets.forEach(function (dataset, i) {
                  chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                      chart.pluginTooltips.push(new Chart.Tooltip({
                          _chart: chart.chart,
                          _chartInstance: chart,
                          _data: chart.data,
                          _options: chart.options.tooltips,
                          _active: [sector]
                      }, chart));
                  });
              });

              // turn off normal tooltips
              chart.options.tooltips.enabled = false;
          }
      },
      afterDraw: function (chart, easing) {
          if (chart.config.options.showAllTooltips) {
              // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
              if (!chart.allTooltipsOnce) {
                  if (easing !== 1)
                      return;
                  chart.allTooltipsOnce = true;
              }

              // turn on tooltips
              chart.options.tooltips.enabled = true;
              Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                  tooltip.initialize();
                  tooltip.update();
                  // we don't actually need this since we are not animating tooltips
                  tooltip.pivot();
                  tooltip.transition(easing).draw();
              });
              chart.options.tooltips.enabled = false;
          }
      }
    });

    var ctx = document.getElementById("myChart").getContext("2d");
    var myPolarChart = new Chart(ctx, {
      type: 'doughnut',
      data: data,
      options: {
        showAllTooltips: true,
        legend: {display: false}
      }
    });
  },
  this.onQueryError = function(error) {
    alert("Error: " + error.code + " " + error.message);
  },
  this.setActionButtons=function() {
    //hud.viewElements['title'].setSubTitle(FragmentTag);
    actions = hud.viewElements['actions'];
    actions.setActionButtonLeft(
      new ActionButton(
        function () {
          this.load("user highscore");
          // showFragment('Highscore',{'id':'map'});
        }.bind(this),
        './img/buttons/score/icon_highscore.png'
      ),true //set this as the active action button
    );
    actions.setActionButtonMiddle(ActionButton.EMPTY);
    actions.setActionButtonRight(
      new ActionButton(
        function () {
          this.load("faction highscore");
          // showFragment('FactionHighscore',{'id':'map'});
        }.bind(this),
        './img/buttons/score/icon_highscorefraction.png'
      )
    );
  },
  this.create = function(name,point,faction,img,id,that, level, rank) {
    args = {
      'name' : name,
      'point' : point,
      'faction' : faction,
      'img' : img,
      'id' : id,
      'level' : level,
      'rank' : rank
    };
    var element = new HighscoreElement(args);
    if(element == null) return;
    that.Elements.push(element);
    that.HighscoreElements['HighscoreElement_'+id]= new Colorable({'id':'HighscoreElement_'+id});
  },
  this.setColor=function(color, initator) {
    for (var stats in this.HighscoreElements) {
      if (this.HighscoreElements[stats].instanceOf("Colorable"))
        this.HighscoreElements[stats].setColor(color, initator);
    }
  };
};
