/**
 * FAQ ... nay comments?
 * Created by yannicklamprecht on 17.08.16.
 */


var Faq = function (args) {

    ViewElement.call(this, args);
    this.addClassName("Faq");

    this.isShown = function () {
        return this.shown;
    },
        this.show = function (args) {
            hud.viewElements['title'].setTitle("System");
            hud.viewElements['title'].setSubTitle("FAQ");
            this.map = document.getElementById('map');


            this.fragment = document.createElement("div");
            this.fragment.id = "fragment";
            this.fragment.classList.add('content_container');
            this.fragment.classList.add('animated');
            this.fragment.classList.add('slideInUp');

            this.fragmentContent = document.createElement("div");
            this.fragmentContent.id = "fragment_content";
            this.fragmentContent.classList.add("faqContainer");


            this.faqTitle = document.createElement("p");
            this.faqTitle.id = "faqTitle";
            this.faqTitle.classList.add("TextColorGradient");

            this.faqTitle.innerHTML = "FAQ";

            this.questionContainer = document.createElement("div");
            this.questionContainer.id = "questionContainer";


            this.fragmentContent.appendChild(this.faqTitle);
            this.fragmentContent.appendChild(this.questionContainer);

            this.fragment.appendChild(this.fragmentContent);

            this.map.appendChild(this.fragment);

            this.container = this.fragmentContent;
            this.divAktuell = this.fragment;
            this.map.style.backgroundImage = "url('./img/kreuz.png')";
            this.map.className = '';
            this.load();
            this.shown = true;
            this.setBackgroundColorByFaction(this.fragment);
        },
        this.close = function (args) {
            this.divAktuell.remove();

            this.shown = false;
        },
        this.load = function () {


            var Faq = Parse.Object.extend("Faq");
            var query = new Parse.Query(Faq);
            query.include("question");
            query.include("answer");
            query.ascending("updatedAt");
            var that = this;
            query.find({
                success: function (results) {
                    that.questionList = [];
                    if (results.length == 0) {
                        $.notify("no campusList found");
                        return;
                    }
                    for (var i = 0; i < results.length; i++) {
                        var object = results[i];
                        var id = object.id;
                        var question = object.get('question').get('deu');
                        var answer = object.get('answer').get('deu');

                        that.questionList.push([id, question, answer, object]);
                    }
                    for (i = 0; i < that.questionList.length; i++) {
                        var questionObject = that.questionList[i];
                        that.addQuestionElement(questionObject[1], questionObject[2]);
                    }
                },
                error: function (error) {
                    $.notify("Error: " + error.code + " " + error.message, "error");
                }
            });

        },
        this.hide = function (args) {
            this.close(args);
        },
        this.setActionButtons = function () {
            actions = hud.viewElements['actions'];
            actions.setActionButtonLeft(new ActionButton(function () {
                showFragment('News');
            }, './img/buttons/news/icon_news.png'));

            actions.setActionButtonMiddle(new ActionButton(function () {
              showFragment('Faq');
            }, './img/buttons/news/icon_fractionstory.png'),true);

            actions.setActionButtonRight(ActionButton.CLOSE);

        },
        this.addQuestionElement = function (questionString, answerString) {
            var div = document.createElement("div");
            div.classList.add("question");

            var question = document.createElement("p");
            question.classList.add("little");

            question.innerHTML = questionString;

            var answer = document.createElement("p");
            answer.classList.add("little");
            answer.innerHTML = answerString;


            div.appendChild(question);
            div.appendChild(answer);
            this.questionContainer.appendChild(div);
        }


};
