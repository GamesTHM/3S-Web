var clock = null;

function startClock() {
    if (!clock) {
        loop();
        clock = setInterval("loop()", autoUpdateTime);
    }
}
function isClockRunning()
{
    if(clock)
    {return true;}
    return false;
}

function loop()
{
    if(settings.getUserSetting('useBrowserGPS')) {
        getLocationFromBrowser(showPositionGPSFromBrowser);
    }
    if(autoUpdateHUD) {
        console.log('Update');
        updateLocations();
        updateHud();
    }
}

function stopClock(reset) {
    if(clock) {
        window.clearInterval(clock);
        clock = null;
    }
}
