/**
 * Created by yannicklamprecht on 30.08.16.
 */


var QuestElement = function (args) {
  ViewElement.call(this, args);

  this.addClassName("QuestElement");

  // this.activeJobId = activeJobId;
  // this.jobCheckIconUrl = jobCheckIconUrl;
  // this.icon = icon;

  this.onClick=function (Tag) {
    console.log('Clicked : ' + Tag);

    var _args = {};
    _args['questDetail_id'] = Tag;
    _args['id']='map';
    _args['activeJobId'] = args.activeJobId;
    _args['jobCheckIconUrl'] = args.jobCheckIconUrl;
    _args['icon'] = args.icon;
    showFragment('QuestDetail',_args);
  };

  this.loadBtnDiv = document.getElementById('loadBtnDiv');

  this.divAktuell = document.createElement('div');

  this.quest_center = document.createElement('div');
  this.quest_left = document.createElement('div');
  this.quest_right = document.createElement('div');

  this.name_container = document.createElement('p');
  this.desc_container = document.createElement('p');

  this.pointContainer = document.createElement('p');
  this.energyContainer = document.createElement('p');

  this.icon_container = document.createElement('div');
  this.image_container = document.createElement('div');
  this.quest_image = document.createElement('img');
  this.jobs_container = document.createElement('div');

  this.questStats = document.createElement('div');

  this.divAktuell.name = args.name;
  this.divAktuell.id = args.id;
  this.divAktuell.className = 'quest';
  this.quest_center.className = 'quest_center';
  this.quest_left.className = 'quest_left';
  this.quest_right.className = 'quest_right';
  this.quest_center.className = 'quest_part quest_center';
  this.quest_left.className = 'quest_part quest_left';
  this.quest_right.className = 'quest_part quest_right';
  this.jobs_container.className = 'job_container';

  this.questStats.className = 'quest_stats';

  this.pointContainer.className = 'pointQuest';
  this.energyContainer.className = 'energyQuest';

  this.name_container.innerHTML = args.name;
  this.name_container.className = 'quest_title TextColorGradient';
  this.desc_container.innerHTML = args.desc;
  this.desc_container.className = 'quest_title_sub';
  this.icon_container.className = 'quest_icon';
  this.icon_container.style.backgroundImage = "url('" + args.icon + "')";
  this.image_container.className = 'quest_image';
  this.quest_image.src = args.image;

  this.points = 0;
  this.energy = 0;

  var jobFinished = args.activeJobId != undefined;
  var jobIconElement, jobIconCheckElement;

  for (i = 0; i < args.jobs_array.length; i++) {
    if (i == 3) break;

    this.currentJob = document.createElement('div');
    this.currentJob.id = 'job' + i;
    this.currentJob.className = 'job';

    jobIconElement = document.createElement('img');
    jobIconElement.className = 'jobIcon';
    this.currentJob.appendChild(jobIconElement);

    this.energy += args.jobs_array[i].get('energyGain');
    this.points += args.jobs_array[i].get('pointGain');

    var jobIcon = args.jobs_array[i].get('icon');
    jobIcon = jobIcon && jobIcon.get('icon');
    jobIcon = jobIcon && jobIcon.url();
    if (jobIcon != null)
      jobIconElement.src = jobIcon;

    if(jobFinished && args.activeJobId != args.jobs_array[i].id) {
      jobIconCheckElement = document.createElement('img');
      jobIconCheckElement.className = 'jobIcon';
      jobIconCheckElement.src = args.jobCheckIconUrl;
      this.currentJob.appendChild(jobIconCheckElement);
    } else if(args.activeJobId == args.jobs_array[i].id) {
      jobFinished = false;
    }

    this.jobs_container.appendChild(this.currentJob);
  }
  this.pointContainer.innerHTML = '<img class="questPointIcon" src="./img/ui/stats/punkte.png"/>' + this.points;
  this.energyContainer.innerHTML = '<img class="questEnergyIcon" src="./img/ui/stats/energie.png"/>' + this.energy;

  this.divAktuell.appendChild(this.quest_left);
  this.divAktuell.appendChild(this.quest_center);
  this.divAktuell.appendChild(this.quest_right);

  this.image_container.appendChild(this.quest_image);

  this.quest_center.appendChild(this.name_container);
  this.quest_center.appendChild(this.desc_container);
  this.quest_left.appendChild(this.icon_container);
  this.quest_left.appendChild(this.image_container);
  this.quest_center.appendChild(this.jobs_container);

  this.questStats.appendChild(this.pointContainer);
  this.questStats.appendChild(this.energyContainer);

  this.quest_center.appendChild(this.questStats)

  this.divAktuell.onclick = function () {
      console.log(args.id);
      this.onClick(args.id);
  }.bind(this);

  // args.parent_container.appendChild(this.divAktuell);
  args.parent_container.insertBefore(this.divAktuell,this.loadBtnDiv);

  this.setBackgroundColorByFaction(this.divAktuell);

  // pushCallback(this.divAktuell);
};
