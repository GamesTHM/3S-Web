var locations_inRange = [null,null,null];
var location_views = [null, null, null];
var location_ListDisable = false;
function updateLocations()
{
  drawLocations('locationChange');
}
function clearLocations()
{
  location_views[0] = document.getElementById('location1');
  location_views[1] = document.getElementById('location2');
  location_views[2] = document.getElementById('location3');
  location_views[0].src = '';
  location_views[1].src = '';
  location_views[2].src = '';
    locations_inRange = [null,null,null];
    if(instance != null)
      instance.refresh('locationChange');
    if(overlayInstance != null)
      overlayInstance.refresh('locationChange')
}
function getLocation(id1,id2,id3,gps,id)
{
  for(var i=0;i<locations_inRange.length;i++)
  {
    if(locations_inRange[i] != null) {
        if (locations_inRange[i][0] != null || locations_inRange[i][1] != null || locations_inRange[i][2] != null) {
            if (locations_inRange[i][0] == id1 && locations_inRange[i][1] == id2 && locations_inRange[i][2] == id3) {
                return i;
            }
        }
        if (id != null) {
            if (locations_inRange[3] == id)
                return i;
        }
        if (gps != null && gps != 0) {
            if(locations_inRange[i][4] != null && locations_inRange[i][4] != 0) {
                var dis = distance(locations_inRange[i][4].longitude, locations_inRange[i][4].latitude, gps.longitude, gps.latitude);
                if (dis <= 20  && dis >= -20){
                    return i;
                }
                else {
                    locations_inRange[i] = null;
                    location_views[pos].src = '';
                }
            }
        }
    }
  }
  return -1;
}
function checkLocationExist(id1,id2,id3,gps,id)
{
  if(getLocation(id1,id2,id3,gps,id) == -1) return false;
  else return true;
}
function checkLocationInRangeWithID(objID)
{
  for(var i=0;i<3;i++)
  {
    if(locations_inRange[i] != null)
    {
      if(locations_inRange[i][3] != null && locations_inRange[i][3] == objID)
      {
        return true;
      }
    }
  }
  return false;
}
function shiftLocations()
{
  locations_inRange[2] = locations_inRange[1]
  locations_inRange[1] = locations_inRange[0];
  locations_inRange[0] = null;
}
function removeLocation(uuid, major, minor, gps) {
  var pos = getLocation(uuid,major,minor,gps);
  if(pos != -1) {
    locations_inRange[pos] = null;
    location_views[pos].src = '';
    console.log('Location removed');

    if(instance != null)
      instance.refresh('locationChange');
    if(overlayInstance != null)
      overlayInstance.refresh('locationChange');

    drawLocations();
  }
  else {
    console.log('Location not removed');
  }


}
function addLocation(id1,id2,id3,gps)
{
  if(checkLocationExist(id1,id2,id3,gps,null)) {
    return;
  }
  //an = new Animation(document.getElementById('location_list'));
  //an.glow(100,3);

    var add = false;
  for(var i=0;i<3;i++)
  {
    if(locations_inRange[i] == null && !add)
    {
      locations_inRange[i] = [id1,id2,id3,null,gps,null];
        console.log('Location added');
      add = true;
      drawLocations();
    }
  }
  if(add == false)
  {
    shiftLocations();
    locations_inRange[0] = [id1,id2,id3,null,gps,null];
    console.log('Location not added');
  }
}
function drawLocations() {
  var locationList = document.getElementById('location_list');
  location_views[0] = document.getElementById('location1');
  location_views[1] = document.getElementById('location2');
  location_views[2] = document.getElementById('location3');

  //if (gpsFromBrowser)
    getLocationImg(0, new Array(location_views[0],location_views[1],location_views[2]));
    getLocationImg(1, new Array(location_views[0],location_views[1],location_views[2]));
    getLocationImg(2, new Array(location_views[0],location_views[1],location_views[2]));
  //else {
    /*
    if(locations_inRange[0] != null)
        getLocationImg(0, new Array(location_views[0]));
    if(locations_inRange[1] != null)
        getLocationImg(1, new Array(location_views[1]));
    if(locations_inRange[2] != null)
        getLocationImg(2, new Array(location_views[2]));
  //}*/
  setBackgroundColorByFaction(locationList);
    locationList.onclick=function(){location_list_OnClick();};
}
function disableLocationList() {
    location_ListDisable = true;
}
function enableLocationList() {
    location_ListDisable = false;
}
function getLocationImg(index, location)
{
  if(locations_inRange[index] == null)
  {
    location.src = '';
    return;
  }
  var mayor = locations_inRange[index][1];
  var minor = locations_inRange[index][2];
  var uuid = locations_inRange[index][0];
  var gps = locations_inRange[index][4];

  var Loc = Parse.Object.extend("Location");
  var query = new Parse.Query(Loc);
  if(gps == null || gps == 0) {
    query.equalTo("beaconmayor", mayor);
    query.equalTo("beaconminor", minor);
    query.equalTo("beaconuuid", uuid);
      query.limit(1);
  }
  else {
    var southwestOfSF = new Parse.GeoPoint((gps.latitude + gpsMeterDiffLat), (gps.longitude - gpsMeterDiffLong));
    var northeastOfSF = new Parse.GeoPoint((gps.latitude - gpsMeterDiffLat), (gps.longitude + gpsMeterDiffLong));
    console.log('southwest:' + southwestOfSF.latitude + ':' + southwestOfSF.longitude);
    console.log('northeast:' + northeastOfSF.latitude + ':' + northeastOfSF.longitude);
    query.withinGeoBox("geopoint", southwestOfSF, northeastOfSF);
      query.limit(3);

    //query.withinKilometers("geopoint", new Parse.GeoPoint(gps), 0.02); //20 Meter
  }
  //query.equalTo("Campus", current_campus);
  query.include('miniImage');
  query.include('LocationCaptureData');
  query.include('name');

  query.find({
    success: function(results) {
      if(results != null)
          if(results.length > 0) {
              for (var i = 0; i < results.length; i++) {
                  if (results.length > 1) {
                      index = i;
                  }
                  var object = results[i];
                  var id = object.id;
                  uuid = object.get('beaconuuid');
                  mayor = object.get('beaconmayor');
                  minor = object.get('beaconminor');

                  //if(checkLocationExist(uuid, mayor, minor, null, id)) return;

                  locations_inRange[index][0] = object.get('beaconuuid');
                  locations_inRange[index][1] = object.get('beaconmayor');
                  locations_inRange[index][2] = object.get('beaconminor');
                  locations_inRange[index][3] = id;

                  var img = object.get('miniImage').get('image');

                  if (img != null) {
                      console.log('url: ' + img.url());
                      location[index].src = img.url();
                  }
                  var capture = object.get('LocationCaptureData');
                  if (capture != null)
                      locations_inRange[index][5] = [img.url(), object.get('name').get('deu'), capture.get('Faction')];
                  else
                      locations_inRange[index][5] = [img.url(), object.get('name').get('deu'), null];
                  pulseMarker(id, 0);

                  //location[index].className = id;
                  location[index].onclick = function () {
                      //location_OnClick(id);
                      location_list_OnClick();
                  };
                  if (instance != null)
                      instance.refresh('locationChange');
                  if (overlayInstance != null)
                      overlayInstance.refresh('locationChange')
              }
          }
        else
              locations_inRange[index] = null;
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}
location_list_OnClick=function(){
    if(location_ListDisable) return;
  console.log('Open Location List');
  var args = {};
  showOverlay('location_list', args);
};
location_OnClick=function(id){

  console.log('Clicked : '+id);
  var args = {};
  args['locationDetail_id'] = id;
  args['id'] = id;
  args['locationInfo_id'] = id;
  showFragment('LocationInfo', args);
};
setBackgroundColorByFaction = function (container)
{
  if(current_faction_color == null)
      current_faction_color = current_faction.get('mainColor');
  container.style.backgroundColor = current_faction_color;
  container.alpha(0.2);
};
