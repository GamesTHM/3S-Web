var QuestDetail =function(args) {
  FullScreenElement.call(this, args);
  this.addClassName("QuestDetail");

  this.shown = false;
  this.id = null;
  this.jobs;
  this.image = '';
  this.icon = args['icon'];
  this.questDetail_id = args['questDetail_id'];
  this.shownStoryPages = args['shownStoryPages'];
  this.activeJobId = args['activeJobId'];
  this.jobCheckIconUrl = args['jobCheckIconUrl'];
  this.finishAktiv = false;
  this.isShown = function () {
    return this.shown;
  },
  this.show = function () {
    new FullScreenElement().show();
    var map = document.getElementById('map');
    map.innerHTML = '<div id="quest_detail"></div>';
    map.style.backgroundImage = "url('./img/kreuz.png')";
    map.className = '';
    this.load();
    this.shown = true;
  },
  this.close = function (showQuestFragment) {
    new FullScreenElement().close();
    this.container = document.getElementById('quest_detail');
    // this.container.classList.add('animated');
    // this.container.classList.add('fadeIn');
    this.container.remove();
    this.shown = false;

    if(showQuestFragment) showFragment("Quest");
  },
  this.load = function () {
    var query = new Parse.Query("Quest");

    query.include(['name','description','image']);
    query.include(['Jobs','Jobs.name','Jobs.description','Jobs.Parameters.LocalizedString',
      'Jobs.shortDescription','Jobs.icon','Jobs.StoryPages']);
    // query.equalTo('objectId', this.questDetail_id);
    query.get(this.questDetail_id).then(
      this.onQuestFetched.bind(this),
      function (error) {
        $.notify("Fehler beim Laden der Quest", "error");
      }
    );
  },
  this.onQuestFetched = function (quest) {
    this.id = quest.id;
    this.name = quest.get('name').get('deu');
    this.desc = quest.get('description').get('content');
    var jobs_array = quest.get('Jobs');

    if(quest.get('image'))
      if(quest.get('image').get('image'))
        this.image = quest.get('image').get('image').url();

    this.create();

    var jobIsFinished = true;

    for (var i = 0; i < jobs_array.length; i++) {
      var object = jobs_array[i];
      var id = object.id;
      var desc = object.get('description').get('deu');
      var jobFunctionName = object.get('functionname');
      var functionParams = object.get('Parameters');
      var storyPages = object.get('StoryPages');

      if(storyPages != null && storyPages.length > 0) {
        if(this.shownStoryPages != null)
        for (z = 0; z < this.shownStoryPages.length; z++) {
          if (storyPages[0].id == this.shownStoryPages[z].id) {
            storyPages = null;
            break;
          }
        }
      }

      var container = '';

      var name = object.get('name').get('deu');
      var desc = object.get('shortDescription').get('deu');
      var icon = object.get('icon').get('icon').url();
      var energy = object.get('energyGain');
      var points = object.get('pointGain');

      if (!this.jobs) {
        this.jobs = [];
      }
      if(i == 0 && !this.activeJobId || id == this.activeJobId)
       jobIsFinished = false;

      var currentJob = new job(id, this.questDetail_id, object,
        jobFunctionName, functionParams, storyPages, container, name,
        points, energy, icon, desc, jobIsFinished, this.jobCheckIconUrl);

      if(i == 0 && !this.activeJobId || id == this.activeJobId)
        this.theActiveJob = currentJob;
      currentJob.create();
    }
  },
  this.create = function () {
    this.container = document.getElementById('quest_detail');
    this.container.classList.add('animated');
    this.container.classList.add('fadeIn');
    this.divAktuell = document.createElement('div');

    var questImage = document.createElement('div');
    questImage.className = 'questImage';
    questImage.style.backgroundImage = 'url('+this.image+')';

    var questIcon = document.createElement('div');
    questIcon.className = 'questIcon';
    questIcon.style.backgroundImage = 'url('+this.icon+')';

    var nameContainer = document.createElement('div');
    nameContainer.className = 'questName TextColorGradient';
    nameContainer.innerHTML = this.name;

    var jobList = document.createElement('table');
    jobList.id = 'job_list';

    var desc_container = document.createElement('p');
    desc_container.innerHTML = this.desc;
    desc_container.className = 'questDetail_content';

    // var abgabe = document.createElement('img');
    // abgabe.src = './img/buttons/icon_check.png';
    // abgabe.onclick = this.abgeben.bind(this);
    // // abgabe.id = id;
    // abgabe.className = 'Abgabe';
    //
    // var close = document.createElement('img');
    // close.src = './img/buttons/icon_close.png';
    // close.onclick = this.close.bind(this,true);
    // // close.id = id;
    // close.className = 'Close';

    this.divAktuell.name = this.name;
    this.divAktuell.id = this.id;
    this.divAktuell.className = 'questDetail';

    this.container.appendChild(this.divAktuell);
    this.divAktuell.appendChild(questImage);
    questImage.appendChild(questIcon);
    this.divAktuell.appendChild(nameContainer);
    this.divAktuell.appendChild(desc_container);
    desc_container.appendChild(jobList);
    this.setBackgroundColorByFaction(this.divAktuell);
  };
  this.abgeben = function () {
    if(!this.finishAktiv) {
      this.finishAktiv = true;
      this.theActiveJob.finishJob();
    }
  };
  this.setActionButtons = function () {
    hud.viewElements['title'].setSubTitle("All");

    actions = hud.viewElements['actions'];
    actions.setActionButtonLeft(
      new ActionButton(
        this.abgeben.bind(this),
        './img/buttons/icon_check.png'
      )
    );
    actions.setActionButtonMiddle(ActionButton.EMPTY);
    actions.setActionButtonRight(
      new ActionButton(
        this.close.bind(this,true),
        './img/buttons/icon_close.png'
      )
    );
  },
  this.resize = function(event) {
    if(isLandscapeMode())
      this.divAktuell.className = 'questDetail fragmentLandscape';
    else
      this.divAktuell.className = 'questDetail'
  }
};
