//global vars
var userMarker;
var allUserMarker = [];
var allMarker = [];

var Map = function (args) {
  ViewElement.call(this,args);
  this.addClassName("News");
  this.shown = false;
  this.menuButton = document.getElementById("map_btn");
  this.map = document.getElementById('map');

  /*this.map_instance = function() {
   return this.map_instance;
   }*/
  this.show = function () {
    //hud.viewElements['title'].setTitle("Karte");
    getLanguageText("map","title","Karte",true, hud.viewElements['title'].title_name);
    //hud.viewElements['title'].setSubTitle("Stockwerk 0");
    getLanguageText("map","subtitle0","Stockwerk 0",true, hud.viewElements['title'].sub_title_name);
    // this.mapButton = document.getElementById("map_btn");
    // this.mapButton.classList.add("active_btn");
    // this.buttonShadow = document.createElement('div');
    // this.buttonShadow.className = 'active_btn';
    // this.mapButton.appendChild(this.buttonShadow);

    if(this.map_instance == null) {
      this.map.innerHTML = '';
      this.map_instance = L.map('map', {
        minZoom: 10,
        zoomControl: false
      }).setView([50.330525, -0.09], 22);
    }
    getLocationFromBrowser(focusUserLocation);

    if(userMarker) {
      userMarker = null;

      if(lastUserLocation) {
        var geolatlong = lastUserLocation.split(";");
        setUserLocation(geolatlong[0],geolatlong[1]);
      }
    }

    if (current_campus != null) {
      var query = new Parse.Query("Campus");

      query.get(current_campus.id).then(
        function (campus) {
          if (campus) {
            var center_point = campus.get('centerPoint');
            var geo_lat = center_point.latitude;
            var geo_long = center_point.longitude;
            if (geo_long || geo_lat) {
              this.map_instance.panTo(L.latLng(geo_lat, geo_long));
              this.map_instance.setZoom(17)
            }
            // console.log('Load Center Point');
          }
        }.bind(this),
        this.onQueryError
      );
    } else {
      this.map_instance.panTo(L.latLng(50.330525, 8.759021));
    }
    //L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.map_instance);
    //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    L.tileLayer('http://george.mnd.thm.de/osm_tiles/{z}/{x}/{y}.png', {
      //attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18,
      id: 'dorstein.onokl79i',
      accessToken: 'pk.eyJ1IjoiZG9yc3RlaW4iLCJhIjoiY2lqanEwM283MDA0N3ZzbHg4aWFqODRqaSJ9.gHmbNGbR8m95rHnTriZOpA'
    }).addTo(this.map_instance);
    //map_instance.on('click', onClickMap);
    this.loadLocations();
    this.shown = true;
  },
  this.hide = function (args) {

  },
  this.close = function (args) {
    // this.buttonShadow.remove();
    if (this.map_instance) {
      // console.log("Clear Map");
      this.map_instance.remove();
      this.map_instance = null;
      this.userMarker = null;
    }
    this.shown = false;
  },
  this.isClosed = function () {

  },
  this.isShown = function () {
      return this.shown;
  },
  this.setActionButtons = function () {
    hud.viewElements['actions'].setActionButtonLeft(
      new ActionButton(
        // function () {
        //   actionButtonsOnClick('Test');
        // },
        undefined,
        './img/buttons/map/stockwerkwechsel_down.png'
      )
    );
    hud.viewElements['actions'].setActionButtonMiddle(
      new ActionButton(
        // function () {
        //   actionButtonsOnClick('Test2');
        // },
        undefined,
        './img/buttons/map/0.png'
      )
    );
    hud.viewElements['actions'].setActionButtonRight(
      new ActionButton(
        // function () {
        //   actionButtonsOnClick('Test3');
        // },
        undefined,
        './img/buttons/map/stockwerkwechsel.png'
      )
    );
  },
  this.resize = function () {

  },
  this.refresh = function () {

  },
  this.reload = function (args) {
    this.loadLocations();
  },
  this.loadLocations = function () {
    if (!this.map_instance) return;
    clearAllLocationMarker();
    var query = new Parse.Query("Location");
    query.equalTo("Campus", current_campus);
    query.equalTo("visibility", true);
    query.include("LocationCaptureData");
    query.include("LocationCaptureData.Faction");
    //query.include("Faction.marker");
    query.exists("LocationCaptureData");

    query.find().then(
      this.onLocationsLoaded.bind(this),
      this.onQueryError
    );
  },
  this.onLocationsLoaded = function (results) {
    allMarker = [];
    for (var i = 0; i < results.length; i++) {
      var object = results[i];
      var id = object.id;
      var geopoint = object.get('geopoint');
      var geo_lat = geopoint.latitude;
      var geo_long = geopoint.longitude;
      var faction_obj = object.get('LocationCaptureData').get('Faction');
      var url = null;
      if (faction_obj != null) {
        var faction_marker = faction_obj.get('marker');
        if (faction_marker != null) {
            url = faction_marker.url();
        }
      } else {
        url = "img/defaultImages/Locationmarker_default.png";
      }
      if (geo_long || geo_lat) {
        if (url != null) {
          var myIcon = L.icon({
            iconUrl: url,
            iconRetinaUrl: url,
            iconSize: [45, 56],
            iconAnchor: [22, 50],
            popupAnchor: [22, 50],
            shadowUrl: url,
            shadowRetinaUrl: url,
            shadowSize: [0, 0],
            shadowAnchor: [0, 0]
          });
          marker = new L.marker([geo_lat, geo_long], {
            icon: myIcon,
            alt: id
          });
        } else {
          marker = new L.marker([geo_lat, geo_long], {
            alt: id
          });
        }
        // console.log(marker._container);
        marker.on('click', this.onClickMap);
        marker.addTo(this.map_instance);
        allMarker.push([id,marker]);
      }
    }
  },
  this.onQueryError = function (error) {
    console.log("Error: " + error.code + " " + error.message);
  },
  this.onClickMap = function (e) {
    var id = this.options.alt;
    if (id) {
      // console.log('Location Clicked : ' + id);
      var args = {};
      args['locationDetail_id'] = id;
      args['id'] = id;
      args['locationInfo_id'] = id;
      args['prevSubTitle'] = hud.viewElements.title.sub_title_name.innerHTML;
      showOverlay('LocationInfo', args);
    }
  }
};

function getMarker(id) {
  for (i=0; i<allMarker.length;i++) {
    if (allMarker[i][0] == id)
      return allMarker[i][1];
  }
  return null;
}
function pulseMarker(id, count) {
  marker = getMarker(id);
  if(marker == null)
      return;
  marker = marker._icon;

  an = new Animation(marker);
  an.pulse(14);
}
function getLocationFromBrowser(callBack) {
  if (navigator.geolocation) {
    console.log("Location from browser.");
    navigator.geolocation.getCurrentPosition(callBack);
  }
  else {
    console.log("Geolocation is not supported by this browser.");
  }
}
function showPositionGPSFromBrowser(position) {
  console.log("Geolocation Browser: " + position.coords.latitude + " : " + position.coords.longitude);
  setUserLocation(position.coords.latitude, position.coords.longitude);
}
function setUserLocation(geo_lat, geo_long) {
  lastUserLocation = geo_lat + ';' + geo_long;
  if (geo_long || geo_lat) {
    var myIcon = L.icon({
      iconUrl: 'img/marker_user.png',
      iconRetinaUrl: 'img/marker_user.png',
      iconSize: [45, 56],
      iconAnchor: [22, 50],
      popupAnchor: [22, 50],
      shadowUrl: 'img/marker_user.png',
      shadowRetinaUrl: 'img/marker_user.png',
      shadowSize: [0, 0],
      shadowAnchor: [0, 0]
    });
    parsePoint = new Parse.GeoPoint({
      latitude: geo_lat * 1,
      longitude: geo_long * 1
    });
    currentUser = Parse.User.current();
    currentUser.set('lastPosition', parsePoint);
    currentUser.set('lastPositionUpdate', new Date());
    currentUser.save(null, { sessionToken : currentUser.getSessionToken() })
    .then(
      function (user) {
        console.log('Update lastPosition');
        addLocation(null, null, null, parsePoint);
      },function (user, error) {
        console.log('Error : ' + error.message);
      }
    );
    if (userMarker) {
      userMarker.setLatLng([geo_lat, geo_long]);
    } else {
      userMarker = L.marker([geo_lat, geo_long], {
        icon: myIcon,
        alt: ''
      });
      userMarker.on('click', map.onClickMap);
      if (instance && instance.map_instance)
        userMarker.addTo(instance.map_instance);
    }
  }
}
function focusUserLocation(position) {
  if (instance.map_instance) { // check if mapinstance exists, cause focus can be called on LocationRefreshButton
    instance.map_instance.setView([position.coords.latitude, position.coords.longitude], 16);
  }
  console.log("focus on map called");
}
function clearAllUserMarker() {
  if (instance && instance.map_instance) {
    for (var i = 0; i < allUserMarker.length; i++) {
      instance.map_instance.removeLayer(allUserMarker[i]);
    }
  }
}
function clearAllLocationMarker() {
  if (instance && instance.map_instance) {
    for (var i = 0; i < allMarker.length; i++) {
      instance.map_instance.removeLayer(allMarker[i][1]);
    }
  }
}
function showAllUsersOnMap() {
  clearAllUserMarker();
  var query = new Parse.Query("User");

  query.exists("lastPosition");
  query.find().then(
    function (results) {
      for (var i = 0; i < results.length; i++) {
        var object = results[i];
        var id = object.id;
        var geopoint = object.get('lastPosition');
        var name = object.get('username');
        var time = object.get('lastPositionUpdate');
        var imgUrl = object.get("image");
        var myIcon;
        if (imgUrl) {
          imgUrl = imgUrl.url();
          myIcon = L.icon({
            iconUrl: imgUrl,
            iconRetinaUrl: imgUrl,
            iconSize: [40, 40],
            iconAnchor: [20, 35],
            popupAnchor: [20, 40],
            shadowUrl: imgUrl,
            shadowRetinaUrl: imgUrl,
            shadowSize: [0, 0],
            shadowAnchor: [0, 0]
          });
        } else {
          myIcon = L.icon({
            iconUrl: 'img/marker_user.png',
            iconRetinaUrl: 'img/marker_user.png',
            iconSize: [45, 55],
            iconAnchor: [22, 50],
            popupAnchor: [22, 50],
            shadowUrl: 'img/marker_user.png',
            shadowRetinaUrl: 'img/marker_user.png',
            shadowSize: [0, 0],
            shadowAnchor: [0, 0]
          });
        }
        console.log('Load lastPosition: ' + name);
        if (myIcon) {
          marker = new L.marker([geopoint.latitude, geopoint.longitude], {
            alt: name,
            icon: myIcon
          });
          myIcon = null;
        }
        marker.bindPopup('User:' + name + ' lastUpdate:' + time).openPopup();
        if (instance && instance.map_instance) {
          marker.addTo(instance.map_instance);
          allUserMarker.push(marker);
        }
      }
    }, function (error) {
      console.log("Error: " + error.code + " " + error.message);
    }
  );
}
