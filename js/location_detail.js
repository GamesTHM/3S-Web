var LocationDetail = function (args) {
    FullScreenElement.call(this, args);

    this.addClassName("LocationDetail");

    this.shown = false;
    this.locationDetailId = args['locationDetail_id'];
    this.type = args['type'];
    this.prevSubTitle = args['prevSubTitle'];

    this.isShown = function () {
        return this.shown;
    },
    this.show = function (args) {
      new FullScreenElement().show();
      this.htmlElement.innerHTML = '<div id="location_detail"></div>';
      this.load('all');
      this.shown = true;
    },
    this.close = function (args) {
      new FullScreenElement().close();
      this.container = document.getElementById('location_detail');
      this.container.innerHTML = '';
      this.container.classList.add('animated');
      this.container.classList.add('fadeIn');
      this.shown = false;
    },
    this.load = function (filter) {
      locationDetailId_temp = this.locationDetailId;
      var container = document.getElementById('location_detail');
      container.innerHTML = '';
      container.classList.add('animated');
      container.classList.add('fadeIn');
      that =this;
      var Loc = Parse.Object.extend("Location");
      var query = new Parse.Query(Loc);
      query.equalTo("objectId", locationDetailId_temp);
      query.include('information');
      query.include('name');
      query.include('lore');
      query.find({
          success: function (results) {
              for (var i = 0; i < results.length; i++) {
                  var object = results[i];
                  var desc = '';
                  var title = '';
                  var headerImage = '';
                  if(that.type != null && that.type == 'story') {
                      if (object.get('lore')) {
                          desc = object.get('lore').get('content');
                      }
                  }
                  else {
                      if (object.get('information')) {
                          desc = object.get('information').get('content');
                      }
                  }

                  if(object.get('name')) {
                     title = object.get('name').get('deu');
                  }
                  if(object.get('headerImage')) {
                      headerImage = object.get('headerImage').get('image').url();
                  }
                  that.create(title,desc,headerImage, locationDetailId_temp);
              }
          },
          error: function (error) {
              alert("Error: " + error.code + " " + error.message);
          }
      });
    },
    this.setActionButtons = function () {
      hud.viewElements['title'].setSubTitle('Detail');
      actions = hud.viewElements['actions'];
      actions.setActionButtonLeft(new ActionButton(function () {
          that.type = 'detail';
          that.load('all');
      },'./img/buttons/icon_info.png'),true);
      actions.setActionButtonMiddle(new ActionButton(function () {
          that.type = 'story';
          that.load('all');
      },'./img/buttons/news/icon_ownstory.png'));
      actions.setActionButtonRight(new ActionButton(function () {
        var args = {
          'locationInfo_id' : that.locationDetailId,
          'prevSubTitle' : that.prevSubTitle
        };
        showFragment('Map');
        showOverlay('LocationInfo',args);
      },'./img/buttons/icon_close.png'));
    },
    this.create = function (title, desc, headerImage, locationDetailId) {
      var container = document.getElementById('location_detail');
      var divAktuell = document.createElement('div');
      var header = document.createElement('div');

      var desc_container = document.createElement('p');
      var title_container = document.createElement('p');

      var close = document.createElement('img');
      close.src = './img/buttons/icon_close.png';
      close.onclick = function () {
          args={'locationInfo_id':locationDetailId};
          showFragment('Map');
          showOverlay('LocationInfo',args);
      };
      close.className = 'Close';

      divAktuell.id = locationDetailId;
      divAktuell.className = 'locationDetail';

      title_container.id = 'locationDetail_title'
      title_container.className = 'TextColorGradient'

      title_container.innerHTML = title;

      desc_container.innerHTML = desc;
      desc_container.className = 'locationDetail_content';

      header.style.backgroundImage = "url('" + headerImage + "')";
      header.id = 'locationDetail_image';

      container.appendChild(divAktuell);
      divAktuell.appendChild(header);
      divAktuell.appendChild(title_container);
      divAktuell.appendChild(desc_container);
      this.container.classList.add('animated');
      this.container.classList.add('fadeIn');
      this.setBackgroundColorByFaction(divAktuell);
    }
};
