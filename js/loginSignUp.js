var indexHTML = "index.html";
var registerHTML = "register.html";
var mainHTML = "main.html";

if(window.location.href.indexOf("-dev.html") != -1) {
  //we're in dev mode
  indexHTML = "index-dev.html";
  registerHTML = "register-dev.html";
  mainHTML = "main-dev.html";
} else {
  //we're in production mode
  appID = 'george.games.thm.de';
  jsKey = 'N4%-_ACs=sAnZr[2';
  serverUrlParse = 'http://george.mnd.thm.de:1337/parse';

  Parse.serverURL = serverUrlParse;
  Parse.initialize(appID, jsKey);

  if(Parse.User.current()) window.location.href = mainHTML; //"main.html";
}

function onEnterPressed(e, from) {
    if (e.keyCode == 13) {
        if(from == 'register')
            register();
        if(from == 'login')
            login();
        return false;
    }
}

function login()
{
    Parse.User.logIn(document.getElementById('username_input').value, document.getElementById('password').value, {
        success: function(user) {
            window.location.href = mainHTML; //"main.html";
        },
        error: function(user, error) {
            $.notify("Input incorrect",'error');
        }
    });
}

function register()
{
    if(document.getElementById('password').value == document.getElementById('password2').value)
    {
        var user = new Parse.User();
        user.set("username", document.getElementById('username_input').value);
        user.set("password", document.getElementById('password').value);
        user.set("studiengang", document.getElementById('study').value);
        user.signUp(null, {
            success: function(user) {
                // Hooray! Let them use the app now.
                Parse.User.logIn(document.getElementById('username_input').value, document.getElementById('password').value, {
                    success: function(user) {
                        window.location.href = mainHTML; //"main.html";
                    },
                    error: function(user, error) {
                        $.notify("Input incorrect",'error');
                    }
                });
            },
            error: function(user, error) {
                // Show the error message somewhere and let the user try again.
                $.notify("Error: " + error.code + " " + error.message,'error');
            }
        });
    }
    else {
        $.notify("Passwords doesnt match",'error');
    }
}

function goToLogin()
{
    window.location.href = indexHTML; //"index.html";
}

function goToRegister()
{
    window.location.href = registerHTML; //"register.html";
}
