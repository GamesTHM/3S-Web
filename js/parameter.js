var Parameter = function (id, text, name, optional, type, job) {
    this.id = id;
    this.text = text;
    this.name = name;
    this.optional = optional;
    this.type = type;
    this.paramFunction = {};
    this.job = job;

    this.tryFinish = function (callback) {
        this.callback = callback;

        if(!this.type) {
          this.callback.finish({});
          return;
        }
        switch (this.type) {
            case 1000: //locations
              var locations = '';
              for (var i = 0; i < 3; i++) {
                if(locations_inRange[i])
                  locations += locations_inRange[i][3] + ';';
              }
              this.paramFunction[this.name] = locations;
              this.callback.finish(this.paramFunction);
              break;
            case 1001: //Usereingabevar
                args = {};
                args['title'] = this.text;
                args['callback'] = this;

                showOverlay('InputDialog',args,true);
                break;
            case 1002: //APIVersion

                break;
            default:  //Error

                break;
        }
    },
    this.finish = function (userEingabe)
    {
        if(userEingabe == null) userEingabe = '';
        if(userEingabe != "") {
            userEingabe = userEingabe.toLowerCase();
            userEingabe = userEingabe.trim();
            this.paramFunction[this.name] = userEingabe;
            this.callback.finish(this.paramFunction);
        }
        instance.finishAktiv = false;
    }

};
