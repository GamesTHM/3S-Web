/**
 * Created by Yannick on 31.03.2016.
 */

/**
 * Create a menu button.
 * @param args
 * @constructor
 */
var MenuButton=function(args){
        ViewElement.call(this,args);
        Colorable.call(this,args);
        this.addClassName("MenuButton");

        var args2 = {'id': args['imageDivId']};
        this.btn = new SubMenuButton(args2);

    this.setColor=function(color,initiator){
        this.btn.setColor(color,initiator);
    }
};

var SubMenuButton=function(args){
    ViewElement.call(this,args);
    Colorable.call(this,args);
    this.addClassName("SubMenuButton");
};