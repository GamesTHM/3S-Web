/**
 * Created by Yannick on 22.03.2016.
 */

/**
 * unused??
 * @param args
 * @constructor
 */
var Quest_Unused = function (args) {
    ViewElement.call(this, args);
    this.addClassName("Quest");
    this.shown = false;
    this.isShown = function () {
        return this.shown;
    },
        this.show = function (args) {
            this.map = document.getElementById('map');
            this.map.innerHTML = '<div id="quest_list" class="content_container"></div>';
            this.map.style.backgroundImage = "url('./img/kreuz.png')";
            this.map.className = '';
            this.load('all');
            this.shown = true;
        }, this.close = function (args) {
        this.container = document.getElementById('quest_list');
        this.container.classList.add('content_container');
        this.container.innerHTML = '';
        this.shown = false;
    },
        this.load = function (filter) {
            hud.viewElements['title'].setSubTitle(filter);

            this.container = document.getElementById('quest_list');
            this.container.classList.add('content_container');
            this.container.innerHTML = '';
            this.Quest = Parse.Object.extend("Quest");
            this.query = new Parse.Query(this.Quest);
            if (filter == 'featured')
                this.query.equalTo('featured', true);
            if (filter == 'daily')
                this.query.greaterThanOrEqualTo('timeToLive', 1)
            this.query.include('name');
            this.query.include('shortDescription');
            this.query.lessThanOrEqualTo('startdate', new Date());
            this.query.greaterThanOrEqualTo("enddate", new Date());
            this.query.equalTo("campus", current_campus);
            this.query.equalTo("visibility", true);
            var that = this;
            this.query.find({
                success: function (results) {
                    if (results.length == 0)
                        $.notify("Keine Quests vorhanden!", "error");
                    for (var i = 0; i < results.length; i++) {
                        var object = results[i];
                        var id = object.id;
                        var name = object.get('name').get('deu');
                        var desc = object.get('shortDescription').get('deu');
                        var user = Parse.User.current();
                        that.checkUserRelation(object, user, name, desc, id, that);
                    }
                },
                error: function (error) {
                    $.notify("Error: " + error.code + " " + error.message, "error");
                }
            });
        }, this.checkUserRelation = function (quest, user, name, desc, id, that) {

        var Relation = Parse.Object.extend("UserQuestRelation");
        var query = new Parse.Query(Relation);
        query.equalTo("quest", quest);
        query.equalTo("user", user);
        query.find({
            success: function (results) {
                if (results.length > 0) {
                    console.log('Quest schon abgeschlossen');
                    return true;
                }
                else {
                    console.log('neuer Quest');
                    that.create(name, desc, id, that);
                    return false;
                }
            },
            error: function (error) {
                $.notify("Error: " + error.code + " " + error.message, "error");
            }
        });
        return false;

    }, this.create = function (name, desc, idd, that) {
        var container = document.getElementById('quest_list');
        var divAktuell = document.createElement('div');

        var name_container = document.createElement('p');
        var desc_container = document.createElement('p');

        divAktuell.name = name;
        divAktuell.id = id;
        divAktuell.className = 'quest';

        name_container.innerHTML = name;
        name_container.className = 'quest_title'
        desc_container.innerHTML = desc;
        desc_container.className = 'quest_title_sub'

        container.appendChild(divAktuell);
        divAktuell.appendChild(name_container);
        divAktuell.appendChild(desc_container);

        divAktuell.onclick = function () {
            that.OnClick(id);
        };
    },
        this.OnClick = function (tag) {
            console.log('Clicked : ' + tag);
            questDetail_id = tag;
            showFragment('QuestDetail');
        }, this.setActionButtons = function () {
        hud.viewElements['title'].setSubTitle('all');
        hud.viewElements['actions'].setActionButtonLeft(new ActionButton(function(){Quest.prototype.load('featured');},'./img/buttons/quest/icon_featuredquest.png'));
        hud.viewElements['actions'].setActionButtonMiddle(new ActionButton(function(){Quest.prototype.load('all');},'./img/buttons/quest/icon_nearquest.png'));
        hud.viewElements['actions'].setActionButtonRight(new ActionButton(function(){Quest.prototype.load('daily');},'./img/buttons/quest/icon_dailyquest.png'));


    }


};