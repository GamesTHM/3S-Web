/**
 * Create/Show players level and levelbar.
 * @param args
 * @constructor
 */
var PlayerLevel = function(args){

    ViewElement.call(this,args);
    this.addClassName("PlayerLevel");

    this.container = document.getElementById('stats');
    if(this.container == null)
        return;

    this.container_center = document.getElementById('stats_center');

    this.levelBar = document.createElement('div');
    this.levelBar.id = 'levelBar';

    this.container.appendChild(this.levelBar);

    this.isShown=function(){
        return this.shown;
    },
    this.show=function(args){
        this.levelBar.style.display = 'block';
        this.refresh();
    },
    this.close=function(args){
        this.map.innerHTML = '';
        this.shown = false;
    },
    this.refresh=function(currentUser){
        if(!currentUser) return;
        var level = this.getLevel();
        var levelProgress = currentUser.get("levelProgress") || 0;

        this.levelBar.style.width = levelProgress*51.5/100 +'vw';
    },
    this.hide=function(args){
        this.levelBar.style.display = 'none';
    },
    this.getLevel=function () {
        var level = currentUser.get("level");

        if(level == null) level = 0;
        return level;
    }
};
