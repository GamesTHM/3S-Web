/**
 * Created by Yannick on 23.03.2016.
 */

/**
 * Create side menu.
 * @param args
 * @constructor
 */
var Menu = function (args) {
    ViewElement.call(this, args);
    Colorable.call(this, args);
    this.addClassName("Menu");
    this.menuButtons = {};
    this.menuButtons['menuButton1'] = new MenuButton({'id': 'menu_button_1', 'imageDivId': 'map_btn'});
    this.menuButtons['menuButton2'] = new MenuButton({'id': 'menu_button_2', 'imageDivId': 'quest_btn'});
    this.menuButtons['menuButton3'] = new MenuButton({'id': 'menu_button_3', 'imageDivId': 'score_btn'});

    this.setActionButtons = function () {
        this.setMenuButton(this.menuButtons['menuButton1'].htmlElement, new ActionButton(function () {
            showFragment('Map');
        }, ''));
        this.setMenuButton(this.menuButtons['menuButton2'].htmlElement, new ActionButton(function () {
            showFragment('Quest');
        }, ''));
        this.setMenuButton(this.menuButtons['menuButton3'].htmlElement, new ActionButton(function () {
            showFragment('Highscore');
        }, ''));
    }, this.setColor = function (color,initiator) {
        for (var menuButton in this.menuButtons) {
            if(this.menuButtons[menuButton].instanceOf("Colorable"))
            this.menuButtons[menuButton].setColor(color,initiator);
        }
    }
};