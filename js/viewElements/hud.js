/**
 * Created by Yannick on 16.03.2016.
 */

/**
 * Um yiss, the HUD. It's ultra hot in here
 * @constructor
 */
var Hud = function () {

    this.viewElements = {};
    this.viewElements['menu'] = new Menu({'id': 'menu'});
    this.viewElements['locations'] = new Locations({'id':'locations'});
    this.viewElements['user'] = new User();
    this.viewElements['faction'] = new FactionButton({'id':'faction'});
    this.viewElements['title'] = new TitleElement({'id':'title'});
    this.viewElements['actions'] = new ActionElement({'id':'actions'});
    this.viewElements['madnessLevel'] = new MadnessLevel({'id': 'madness'});
    this.viewElements['playerLevel'] = new PlayerLevel({'id': 'levelBar'});

    this.enterFullScreen = function () {
        var filter = new Array();
        filter.push('menu');
        filter.push('locations');
        this.hideWithFilter(filter);
    },
    this.exitFullScreen = function () {
        this.show();
    },
    this.hideWithFilter = function (filter) {
        for(i=0; i< filter.length; i++) {
            this.viewElements[filter[i]].hide();
        }
    },
    this.hide = function () {

        for( var element in this.viewElements){
            this.viewElements[element].hide();
        }
    }, this.show = function () {
        for( var element in this.viewElements){
            this.viewElements[element].show();
        }
    }, this.getElementByAssocindex = function (assocIndex) {
        return this.viewElements[assocIndex];
    },this.resize=function(){
        $("#actions").width($("#title").width());
        $("#actions").height($("#title").height());


        $("#action_1").width($("#title").width() / 100 * 10);
        $("#action_2").width($("#title").width() / 100 * 10);
        $("#action_3").width($("#title").width() / 100 * 10);
        $("#action_1").height($("#title").width() / 100 * 10);
        $("#action_2").height($("#title").width() / 100 * 10);
        $("#action_3").height($("#title").width() / 100 * 10);


        $("#username").css({bottom: $("#stats").height() / 1.7});

        $("#madness").css({top: $("#actions").height() - 10 });
        $("#madness").css({width: $("#actions").width() - 5 });
        $("#madness").css({height: $("#actions").height() / 2 });

        for(var element in this.viewElements) {
            if(this.viewElements[element].instanceOf("viewElement"))
            this.viewElements[element].resize();
        }

    },this.setColor=function(color,initator){
        for(var element in this.viewElements){
           if(this.viewElements[element].instanceOf("Colorable"))
               this.viewElements[element].setColor(color,initator);
        }
        if(this==initator)paintBrush.processFilters();
    }
};