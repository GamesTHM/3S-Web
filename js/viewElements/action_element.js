/**
 * Created by Yannick on 29.03.2016.
 */

/**
 * Create an ActionElement ??
 * @param args
 * @constructor
 */
var ActionElement = function (args) {
  ViewElement.call(this, args);
  Colorable.call(this, args);
  this.addClassName("ActionElement");

  this.actions = {};
  this.actions['actionDown'] = document.getElementById('action_1');
  this.actions['actionMiddle'] = document.getElementById('action_2');
  this.actions['actionUp'] = document.getElementById('action_3');

  this.setActionButtonLeft = function (actionButton,isActive) {
    this.setActionButton(this.actions.actionDown, actionButton,isActive);
  },
  this.setActionButtonRight = function (actionButton,isActive) {
    this.setActionButton(this.actions.actionUp, actionButton,isActive);
  },
  this.setActionButtonMiddle = function (actionButton,isActive) {
    this.setActionButton(this.actions.actionMiddle, actionButton,isActive);
  },
  this.setActionButton = function (actionB, actionButton,isActive) {
    if(isActive) {
      this.active = actionB;
      this.active.className = "active_btn";
    } else actionB.className = undefined;

    actionB.src = actionButton.getImagePath();
    if(typeof actionButton.getOnClickElement() == "function") {
      actionB.onclick = function(actionB,actionButton) {
        if(this.active != actionB) {
          actionB.className = "active_btn";
          if(this.active) this.active.className = undefined;
          this.active = actionB;
        }
        actionButton.getOnClickElement().call();
      }.bind(this,actionB,actionButton);
    } else {
      actionB.onclick = undefined;
    }
  }
};
