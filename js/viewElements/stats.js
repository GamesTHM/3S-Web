/**
 * Created by Yannick on 23.03.2016.
 */

/**
 * Setting the stats of an Player.
 * @param args
 * @constructor
 */
var Stats=function(args){
  ViewElement.call(this,args);
    this.addClassName("Stats");

  this.statsElements={};
    Colorable.call(this,{'id':'stats_center'});
    this.statsElements['statsCenter']= new Colorable({'id':'stats_center'});
    this.statsElements['statsLeft']= new Colorable({'id':'stats_left'});
    this.statsElements['statsRight']= new Colorable({'id':'stats_right'});
    this.energy= new ViewElement({'id':'energy'});
    this.points= new ViewElement({'id':'point'});
    this.hide = function (args) {
      for (var stats in this.statsElements) {
        this.statsElements[stats].hide();
      }
    },this.setEnergy=function(energy){
      if(energy == null)
          energy = 0;
        this.energy.setTitle(Math.floor(energy));
    },this.setPoints=function(points){
      if(points == null)
          points = 0;
        this.points.setTitle(Math.floor(points));
    },this.setColor=function(color, initator) {
      for (var stats in this.statsElements) {
        if (this.statsElements[stats].instanceOf("Colorable"))
          this.statsElements[stats].setColor(color, initator);
      }
    }
};