/**
 * Created by Yannick on 29.03.2016.
 */

/**
 * Create an ActionButton element, with a specific icon?
 * @param onClick
 * @param imagePath
 * @constructor
 */
var ActionButton = function(onClick,imagePath) {
  this.onClickElement=onClick;
  this.imagePath=imagePath;

  this.getImagePath = function() {
    return this.imagePath;
  },
  this.getOnClickElement=function() {
    return this.onClickElement;
  }
};

ActionButton.EMPTY = new ActionButton('','');
ActionButton.CLOSE = new ActionButton(
  function() {
    var args = {};
    args['id'] = 'map';
    showFragment('Map', args);
  },
  './img/buttons/icon_close.png'
);
