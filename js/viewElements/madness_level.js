/**
 * Updates the madness level.
 * @param args
 * @constructor
 */
var MadnessLevel = function(args){

    this.container = document.getElementById('main');

    this.madness = document.createElement('div');
    this.madnessBar = document.createElement('div');
    this.madnessBar.id = 'madnessBar';
    this.madness.id = 'madness';
    this.madnessBar.className = 'progressbar';

    this.container.appendChild(this.madness);
    this.madness.appendChild(this.madnessBar);

    ViewElement.call(this,args);
    this.addClassName("MadnessLevel");
    Colorable.call(this,{'id':'madness'});

    //this.hide();

    this.isShown=function(){
        return this.shown;
    }/*,this.show=function(args){
        this.refresh();
    }*/,this.close=function(args){
        this.map.innerHTML = '';
        this.shown = false;
    },this.refresh=function(currentUser){

        // currentUser = Parse.User.current();
        // currentUser.fetch();
        madness = currentUser.get("madness")

        if(madness == null)
            madness = 0;
        this.madnessBar.style.width = (madness%1 * 100) +'%';

    }/*,this.hide=function(args){
        this.close(args);
    }*/
};
