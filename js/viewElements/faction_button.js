/**
 * Created by Yannick on 29.03.2016.
 */

/**
 * Create a Faction button?
 * @param args
 * @constructor
 */
var FactionButton = function (args) {
    ViewElement.call(this, args);
    this.addClassName("FactionButton");
    this.factionId = args['factionId'];


    this.updateFactionStyle = function () {
        var Fac = Parse.Object.extend("Faction");
        var query = new Parse.Query(Fac);
        query.equalTo("objectId", this.factionId);
        query.find({
            success: function (results) {
                for (var i = 0; i < results.length; i++) {
                    var object = results[i];
                    var id = object.id;
                    faction_img = object.get('logo');
                    if (faction_img != null) {
                        console.log('url: ' + faction_img.url());
                        document.getElementById('faction').style.backgroundImage = "url('" + faction_img.url() + "')";
                    }
                }
            },
            error: function (error) {
                alert("Error: " + error.code + " " + error.message);
            }
        });
    },this.setFactionId = function (factionId, styleShouldUpdate) {
        this.factionId = factionId;
        if (styleShouldUpdate != undefined && styleShouldUpdate)this.updateFactionStyle();
    }
};