/**
 * Create a storydialog container.
 * @param args
 * @constructor
 */
var StoryDialog = function(args){
    ViewElement.call(this,args);
    this.addClassName("StoryDialog");
    this.storyPages = args['StoryPages'];
    this.questID = args['questDetail_id'];
    this.activeJobId = args['activeJobId'];
    this.jobCheckIconUrl = args['jobCheckIconUrl'];

    this.first = true;
    this.last = false;

    this.isShown=function(){
        return this.shown;
    },
        this.show=function(args){
            this.map = document.getElementById('map');
            this.map.innerHTML='<div id="storyDialog" class="viewElements"></div>';
            this.map.style.backgroundImage = "url('./img/kreuz.png')";
            this.map.className = '';
            this.container = document.getElementById('storyDialog');
            this.load();
            this.shown = true;
        }, this.close=function(args){
        this.map.innerHTML = '';
        this.shown = false;
    }, this.load=function(){

        //load Background Grafik

        //load Characktervar
        this.Charackter = new StoryCharackter('storyDialog');
        this.Charackter.create();
        //load Messages
        this.SpeechBubble = new SpeechBubble('storyDialog');
        this.SpeechBubble.create();
        this.SpeechBox = new SpeechBox('storyDialog');
        this.SpeechBox.create();

        this.loadStoryPageContent();

    },this.loadStoryPageContent = function() {
        this.speechBubbleTexts = new Array();
        this.boxText = new Array();
        this.bg = new Array();
        this.character = new Array();

        for(i=0; i<this.storyPages.length;i++) {
            var StoryPage = Parse.Object.extend("StoryPage");
            var query = new Parse.Query(StoryPage);
            query.include('bubbleText');
            query.include('boxText');
            query.include('background');
            query.include('narrator');
            query.equalTo('objectId', this.storyPages[i].id);
            var that = this;
            query.find({
                success: function (results) {
                    for (var i = 0; i < results.length; i++) {
                        txt = results[i].get('bubbleText');
                        if(txt!= null)
                            txt = results[i].get('bubbleText').get('deu');
                        that.speechBubbleTexts.push(txt);

                        txt = results[i].get('boxText');
                        if(txt!= null)
                            txt = results[i].get('boxText').get('deu');
                        that.boxText.push(txt);

                        that.bg.push(results[i].get('background'));
                        that.character.push(results[i].get('narrator'));

                        that.SpeechBubble.setContent(that.speechBubbleTexts);
                        that.SpeechBox.setContent(that.boxText);

                        if(that.first) {
                            that.next();
                            that.first = false;
                        }
                    }
                }
            });
        }
    },this.hide=function(args){
        this.close(args);
    },this.setActionButtons = function() {
        var that = this;
        actions = hud.viewElements['actions'];
        actions.setActionButtonLeft(new ActionButton(function () {
            that.prev();
        }, './img/buttons/icon_back.png'));
        actions.setActionButtonMiddle(new ActionButton(function () {

        }, ''));
        if(!this.last) {
            actions.setActionButtonRight(new ActionButton(function () {
                that.next();
            }, './img/buttons/icon_next.png'));
        }
        else
        {
            actions.setActionButtonRight(new ActionButton(function () {
                var args = {};
                args['questDetail_id'] = that.questID;
                args['shownStoryPages'] = that.storyPages;
                args['activeJobId'] = that.activeJobId;
                args['jobCheckIconUrl'] = this.jobCheckIconUrl;
                args['id']='map';
                showFragment('QuestDetail',args);
            }, './img/buttons/icon_close.png'));
        }
    },this.next = function(){
        this.SpeechBubble.next();
        this.SpeechBox.next();

    },this.prev = function(){
        this.SpeechBubble.prev();
        this.SpeechBox.prev();
    }
};
/**
 * Put a story charackter in the container?
 * @param container
 * @constructor
 */
var StoryCharackter = function(container) {
    ViewElement.call(this,{'id':container});
    this.addClassName("StoryCharackter");
    this.create = function(){
        this.container = document.getElementById(container);
        this.charackter = document.createElement('div');
        this.charackter.id = 'storyCharackter';
        this.charackter.className = 'bgImage';

        this.container.appendChild(this.charackter);

    }
};
/**
 * Add a speechbubble to the container?
 * @param container
 * @constructor
 */
var SpeechBubble = function(container) {
    ViewElement.call(this,{'id':container});
    this.addClassName("SpeechBubble");
    this.pos = -1;
    this.textContent = new Array();
    this.create = function(){
        this.container = document.getElementById(container);
        this.text = document.createElement('p');
        this.bubble = document.createElement('div');
        this.bubble.id = 'storyBubble';
        this.bubble.className = 'bgImage';

        this.bubble.appendChild(this.text);
        this.container.appendChild(this.bubble);

    },this.hide = function(){
        this.bubble.style.display = 'none';
    },this.show = function(){
        this.bubble.style.display = '';
    },this.setContent = function(txtArray){
        this.textContent = txtArray;

    },this.next = function(){
        if(this.textContent.length > (this.pos+1)) {
            instance.last = false;
            instance.setActionButtons();
            this.pos++;
        }

        if(this.textContent.length == (this.pos+1) && !instance.first)
        {
            instance.last = true;
            instance.setActionButtons();
        }

        if(this.textContent[this.pos] == null)
            this.hide();
        else
            this.show();

        this.text.innerHTML = this.textContent[this.pos];

    },this.prev = function(){
        if((this.pos-1)>=0) {
            instance.last = false;
            instance.setActionButtons();
            this.pos--;
        }

        if(this.textContent[this.pos] == null)
            this.hide();
        else
            this.show();

        this.text.innerHTML = this.textContent[this.pos];
    }
};
var SpeechBox = function(container) {
    ViewElement.call(this,{'id':container});
    this.addClassName("SpeechBox");
    this.pos = 0;
    this.textContent = new Array();
    this.create = function(){
        this.container = document.getElementById(container);
        this.box = document.createElement('div');
        this.text = document.createElement('p');
        this.box.id = 'storyBox';
        this.box.className = 'bgImage';

        this.box.appendChild(this.text);
        this.container.appendChild(this.box);

    },this.hide = function(){
        this.box.style.display = 'none';
    },this.show = function(){
        this.box.style.display = '';
    },
    this.setContent = function(txtArray){
        this.textContent = txtArray;

    },this.next = function(){
        if(this.textContent.length > (this.pos+1))
            this.pos++;

        if(this.textContent[this.pos] == null)
            this.hide();
        else
            this.show();

        this.text.innerHTML = this.textContent[this.pos];

    },this.prev = function(){
        if((this.pos-1)>=0)
            this.pos--;

        if(this.textContent[this.pos] == null)
            this.hide();
        else
            this.show();

        this.text.innerHTML = this.textContent[this.pos];
    }
};
