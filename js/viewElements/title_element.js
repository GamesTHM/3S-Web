/**
 * Created by Yannick on 21.03.2016.
 */

/**
 * Some title thingy.
 * @param args
 * @constructor
 */
var TitleElement = function (args) {
    ViewElement.call(this, args);
    SubTitle.call(this, args);
    Colorable.call(this,args);
    this.addClassName("TitleElement");

    this.title_name = document.getElementById('title_name');
    this.sub_title_name = document.getElementById('title_sub');

    this.setTitle = function (message) {
        this.title_name.innerHTML =  message;//replaceAllUmlautsWithoutUpperCase(message).toUpperCase();

    },
    this.setSubTitle = function (message) {
        this.sub_title_name.innerHTML = message;//replaceAllUmlautsWithoutUpperCase(message).toUpperCase();
    }
};
