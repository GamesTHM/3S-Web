/**
 * Created by Yannick on 23.03.2016.
 */

/**
 * User
 * @param args
 * @constructor
 */
var User = function (args) {
    ViewElement.call(this, args);
    Colorable.call(this, args);
    this.addClassName("User");

    this.userElements = {};
    this.userElements['userName'] = new ViewElement({'id': 'username'});
    this.userElements['picture'] = new ViewElement({'id': 'picture'});
    this.userElements['stats'] = new Stats({'id': 'stats'});

    this.hide = function (args) {
        for (var element in this.userElements) {
            this.userElements[element].hide(args);
        }
    },
    this.show = function (args) {
        for (var element in this.userElements) {
            this.userElements[element].show(args);
        }
    },
    this.setUserName = function (username, level) {
        this.userElements['userName'].htmlElement.innerHTML = username + ' [' + (level || 0) + ']';
    },
    this.setPicture = function (picture) {
      croppingImg(picture, this.userElements['picture'].htmlElement, 'circle', setUserImg);
    },
    this.setColor = function (color, initator) {
        for (var element in this.userElements) {
            if (this.userElements[element].instanceOf("Colorable"))
                this.userElements[element].setColor(color, initator);
        }
    },
    this.getProfilePictureLink = function () {
        return this.hasProfilePicture() ? currentUser.get("image").url() : "img/defaultImages/Placeholder_profilpic.png";
    },
    this.hasProfilePicture = function () {
        return currentUser.get("image") != undefined;
    },
    this.getUserImageData = function () {
        return this.userElements['picture'].htmlElement.style.backgroundImage;
    },
    this.getUserImageDataNoUrlTag = function () {
        return this.getUserImageData().replace("url(\"", "").replace("\")", "");
    },
    this.getName=function () {
        return currentUser.get("username");
    }
};
