/**
 * Created by Yannick on 22.03.2016.
 */
/**
 * Generate a highscore element.
 * @param args
 * @returns {*}
 * @constructor
 */
var HighscoreElement = function(args) {
    //Is there all Data?
    if(
      args['name'] == null
      || args['point'] == null
      || args['faction'] == null
      || args['id'] == null
      || args['level'] == null
      || args['rank'] == null
    ) return null;

    ViewElement.call(this, args);
    this.addClassName("HighscoreElement");

    this.loadBtnDiv = document.getElementById('loadBtnDiv');

    this.create = function(name,point,faction,img,id,level, rank){

        var container = document.getElementById('score_list');
        var divAktuell = document.createElement('div');

        var img_container = document.createElement('span');
        var score_container = document.createElement('div');
        var point_container = document.createElement('p');
        var rank_container = document.createElement('p');
        var name_container = document.createElement('p');
        var faction_container = document.createElement('span');
        var left_div = document.createElement('div');
        var middle_div = document.createElement('div');
        var right_div = document.createElement('div');

        container.className = 'content_container animated fadeInUp';

        divAktuell.name = name;
        divAktuell.id = 'HighscoreElement_'+id;
        divAktuell.className = 'score';


        left_div.className = 'score_left';
        right_div.className = 'score_right';

        name_container.innerHTML = name + ' [' + level + ']';
        name_container.className = 'score_title TextColorGradient';
        score_container.className = 'score_container';
        point_container.innerHTML = point;
        point_container.className = 'score_point';
        rank_container.innerHTML = rank + '.';
        rank_container.className = 'score_rank';
        img_container.id = 'score_img_'+id;
        img_container.className = 'score_img score_left';
        faction_container.className = 'score_faction score_right';
        middle_div.className = 'score_middle';

        if(img != null)
            img_container.style.backgroundImage = "url('"+img.url()+"')";
        if(faction != null)
            faction_container.style.backgroundImage = "url('"+faction.get('logo').url()+"')";

        score_container.appendChild(rank_container);
        score_container.appendChild(point_container);

        // container.appendChild(divAktuell);
        container.insertBefore(divAktuell,this.loadBtnDiv);
        left_div.appendChild(img_container);
        divAktuell.appendChild(left_div);
        middle_div.appendChild(name_container);
        middle_div.appendChild(score_container);
        // middle_div.appendChild(rank_container);
        // middle_div.appendChild(point_container);
        divAktuell.appendChild(middle_div);
        divAktuell.appendChild(right_div);
        right_div.appendChild(faction_container);

        this.setBackgroundColorByFaction(divAktuell);
    },this.resize = function (event) {

    };


    this.create(args['name'],args['point'],args['faction'],args['img'],args['id'],args['level'],args['rank']);

    return this;
};
