var FullScreenElement = function (args) {
    ViewElement.call(this, args);
    this.addClassName("FullScreenElement");

    this.show = function (args) {
        hud.enterFullScreen();
    }, this.close = function (args) {
        hud.exitFullScreen();
    };
}