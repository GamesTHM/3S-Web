/**
 * Created by Yannick on 31.03.2016.
 */

/**
 * ???
 * @param args
 * @constructor
 */
var Colorable = function (args) {
    ViewElement.call(this, args);
    this.addClassName("Colorable");

    this.setColor = function (color, initator) {

        paintBrush.destroyStash(this.htmlElement, true);
        if (!this.htmlElement.classList.contains('filter-tint')) {
            this.htmlElement.classList.add('filter-tint');
            this.htmlElement.setAttribute('data-pb-tint-opacity', '0.2');
        }

        if (color != undefined)
            this.htmlElement.setAttribute('data-pb-tint-colour', color);
        if (initator == this)paintBrush.processFilters();
    }

};
