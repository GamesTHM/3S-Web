var Story = function(args){
  ViewElement.call(this,args);
  this.addClassName("Story");

  this.isShown=function(){
    return this.shown;
  },
      this.show=function(args){
          hud.viewElements['title'].setTitle("System");
          hud.viewElements['title'].setSubTitle("Story");
        this.map = document.getElementById('map');
        this.map.innerHTML='<div id="fragment"><div id="fragment_content"></div></div>';
        this.map.style.backgroundImage = "url('./img/kreuz.png')";
        this.map.className = '';
        this.container = document.getElementById('fragment_content');
        this.load();
        this.shown = true;
          this.frag = document.getElementById('fragment');
          this.setBackgroundColorByFaction(this.frag);
      }, this.close=function(args){
    this.map.innerHTML = '';
    this.shown = false;
  }, this.load=function(){
    getWebElementFromCloud('story',this.container);
  },this.hide=function(args){
    this.close(args);
  },this.setActionButtons = function() {
    actions = hud.viewElements['actions'];
    actions.setActionButtonLeft(new ActionButton(function () {
      showFragment('News');
    }, './img/buttons/news/icon_news.png'));
    actions.setActionButtonMiddle(new ActionButton(function () {
      showFragment('Faction');
    }, './img/buttons/news/icon_fractionstory.png'));
    actions.setActionButtonRight(new ActionButton(function () {
      showFragment('Story');
    }, './img/buttons/news/icon_ownstory.png'));
  }
};
