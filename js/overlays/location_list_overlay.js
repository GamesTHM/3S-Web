/**
 * Created by danielhahn on 28.07.16.
 */
var LocationListOverlay = function (args) {
    Overlay.call(this,args);
    this.addClassName("LocationListOverlay");
    this.prevSubTitle = hud.viewElements.title.sub_title_name.innerHTML;

    this.isShown=function(){
        return this.shown;
    },
        this.show=function(args){
            hud.viewElements['title'].setTitle("Karte");
            hud.viewElements['title'].setSubTitle("Locations");
            this.map = document.getElementById('main');
            this.container = document.createElement('div');
            this.container.id = 'overlay';
            this.container.className = 'animated fadeIn'
            this.divAktuell = document.createElement('div');
            this.divAktuell.id = 'overlay_content';
            this.map.appendChild(this.container);
            this.container.appendChild(this.divAktuell);
            this.load();
            this.shown = true;
            this.resize();
            this.setBackgroundColorByFaction(this.container);
            this.htmlElement = this.container;
        },
    this.close=function(args){
      hud.viewElements.title.setSubTitle(this.prevSubTitle);
        this.container.className = 'animated fadeOut'
      this.map.removeChild(this.container);
      this.shown = false;
    }, this.load=function(){
        for(i=0;i<locations_inRange.length;i++)
        {
            if(locations_inRange[i] == null || locations_inRange[i][5] == null)
                continue;

            var img1 = locations_inRange[i][5][0];
            var nameAkt = locations_inRange[i][5][1];
            var img2 = locations_inRange[i][5][2];
            var locId  = locations_inRange[i][3];
            this.drawListElement(img1,nameAkt,img2, locId);
        }
    },this.hide=function(args){
        this.close(args);
    },this.setActionButtons = function() {
      hud.viewElements['actions'].setActionButtonLeft(
        new ActionButton(
          // function () {
          //   actionButtonsOnClick('Test');
          // },
          undefined,
          './img/buttons/map/stockwerkwechsel_down.png'
        )
      );
      hud.viewElements['actions'].setActionButtonMiddle(
        new ActionButton(
          // function () {
          //   actionButtonsOnClick('Test2');
          // },
          undefined,
          './img/buttons/map/0.png'
        )
      );
      hud.viewElements['actions'].setActionButtonRight(
        new ActionButton(
          // function () {
          //   actionButtonsOnClick('Test3');
          // },
          undefined,
          './img/buttons/map/stockwerkwechsel.png'
        )
      );
    },this.resize = function(event)
    {
        if(isLandscapeMode())
            this.divAktuell.className = 'fragmentLandscape';
        else
            this.divAktuell.className = ''
    },this.drawListElement = function(img1, nameAkt, img2, locId)
    {
        var that = this;
        var currentElement = document.createElement('div');

        var img_container = document.createElement('span');
        var name_container = document.createElement('p');
        var faction_container = document.createElement('span');
        var left_div = document.createElement('div');
        var middle_div = document.createElement('div');
        var right_div = document.createElement('div');

        currentElement.name = nameAkt;
        currentElement.id = locId;
        currentElement.className = 'score';


        left_div.className = 'score_left openOverlay';
        right_div.className = 'score_right openOverlay';

        name_container.innerHTML = nameAkt;
        name_container.className = 'score_title TextColorGradient openOverlay';
        img_container.id = 'score_img_'+locId;
        img_container.className = 'score_img score_left openOverlay';
        faction_container.className = 'score_faction score_right openOverlay';
        middle_div.className = 'score_middle openOverlay';

        if(img1 != null)
            img_container.style.backgroundImage = "url('"+img1+"')";
        if(img2 != null)
            faction_container.style.backgroundImage = "url('"+img2.get('logo').url()+"')";

        this.divAktuell.appendChild(currentElement);
        left_div.appendChild(img_container);
        currentElement.appendChild(left_div);
        middle_div.appendChild(name_container);
        currentElement.appendChild(middle_div);
        currentElement.appendChild(right_div);
        right_div.appendChild(faction_container);

        currentElement.onclick = function(){that.location_OnClick(currentElement.id);};
        this.setBackgroundColorByFaction(currentElement);
    },this.location_OnClick = function(id){
      console.log('Clicked : '+id);
      var args = {};
      args['locationDetail_id'] = id;
      args['id'] = id;
      args['locationInfo_id'] = id;
      args['prevSubTitle'] = this.prevSubTitle;
      showOverlay('LocationInfo', args);
    }
}
