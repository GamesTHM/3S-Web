/**
 * Created by danielhahn on 28.07.16.
 */
var InputDialog = function (args) {
    Overlay.call(this,args);
    this.addClassName("InputDialog");
        this.show = function () {
            this.callback = args['callback'];
            var that = this;

            this.actionButton1 = document.getElementById('action_1');
            this.actionButton3 = document.getElementById('action_3');
            this.actionButton1.style.display = "none";
            this.actionButton3.style.display = "none";

            this.container = document.createElement('div');
            this.container.id = 'InputDialog';
            this.container.title = '';
            this.containerInhalt = document.createElement('div');
            this.containerInhalt.className = 'dialog-inner';
            this.containerTitleText = document.createElement('div');
            this.containerTitleText.id = 'InputDialogTitle';
            this.containerTitleText.className = 'TextColorGradient';
            this.containerTitleText.innerHTML = args['title'];
            this.containerInput = document.createElement('input');

            var btnContainer = document.createElement('div');
            btnContainer.id = 'btnContainer';
            this.btnOK = document.createElement('div');
            this.btnOK.id = 'btnOK';
            this.btnOK.style.backgroundImage = 'url(./img/buttons/icon_check.png)';
            this.btnOK.onclick = function () {that.btnOKClick();};
            this.btnCancel = document.createElement('div');
            this.btnCancel.id = 'btnCancel';
            this.btnCancel.style.backgroundImage = 'url(./img/buttons/icon_close.png)';
            this.btnCancel.onclick = function () {that.btnCancelClick();};

            document.body.appendChild(this.container);
            this.container.appendChild(this.containerInhalt);
            this.containerInhalt.appendChild(this.containerTitleText);
            this.containerInhalt.appendChild(this.containerInput);

            btnContainer.appendChild(this.btnOK);
            btnContainer.appendChild(this.btnCancel);
            this.containerInhalt.appendChild(btnContainer);

            // this.setBackgroundColorByFaction(this.container);
            this.shown = true;
        },
        this.close = function(args) {
            document.body.removeChild(this.container);
            this.shown = false;
        },
        this.showDialog = function (args) {
            this.show();
            return this.getInput();
        },
        this.setActionButtons = function () {

        },
        this.resize = function (event) {

        },
        this.refresh = function (event) {

        },
        this.reload = function () {

        },
        this.getInput = function() {
            this.input = this.containerInput.value;
            if (this.input == null) this.input = '';
            return this.input;
        },
        this.showActionButtons = function() {
          this.actionButton1.style.display = "block";
          this.actionButton3.style.display = "block";
        },
        this.btnOKClick = function () {
            if(this.callback != null)
                this.callback.finish(this.getInput());
            this.showActionButtons();
            closeOverlay();
        },
        this.btnCancelClick = function () {
            if(this.callback != null)
                this.callback.finish('');
            this.showActionButtons();
            closeOverlay();
        },
        this.onFocusLeave = function (obj) {
          if(
            this.isShown()
            && !((' ' + obj.id + ' ').indexOf('action_') > -1)
            && obj.parentNode && !isDescendant(this.container,obj)
          ) {
            console.log('no Focus on : ' + this.classes[0]);
            if(this.callback != null) this.callback.finish('');
            this.showActionButtons();
            closeOverlay();
          }
        };
};

//helper function
function isDescendant(parent, child) {
     var node = child.parentNode;
     while (node != null) {
         if (node == parent) {
             return true;
         }
         node = node.parentNode;
     }
     return false;
}
