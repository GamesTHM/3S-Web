/**
 * Created by danielhahn on 28.07.16.
 */
var Overlay = function (args) {
    this.classes = new Array("Overlay");
    if (args != undefined) {
        if (args['id'] != undefined)this.htmlElement = document.getElementById(args['id']);
    }
    this.shown = false;

        this.show = function (args) {
            this.htmlElement.style.display = 'block';
            this.shown = true;
        },

        this.hide = function (args) {
            this.htmlElement.style.display = 'none';
            this.shown = false;
        },
        this.close = function (args) {

        },
        this.isClosed = function () {

        },
        this.isShown = function () {
            return this.shown;
        },
        this.setActionButtons = function () {

        },
        this.resize = function (event) {

        },
        this.refresh = function (event) {

        },
        this.reload = function () {

        },
        this.setTitle = function (title) {
            this.htmlElement.innerHTML = title;
        },
        this.addClassName = function (className) {
            this.classes.push(className);
        },
        this.instanceOf = function (className) {
            return this.classes.indexOf(className) != -1;
        },
        this.resize = function (event) {
            console.log('resize : ' + this.classes[1]);
        },
        this.setBackgroundColorByFaction = function (container)
        {
            if(current_faction_color == null)
                current_faction_color = current_faction.get('mainColor');
            container.style.backgroundColor = current_faction_color;
            container.alpha(0.9);
        },
        this.onFocusLeave = function (obj) {
            if(this.isShown() && (obj.id != 'location_list'
                && !((' ' + obj.id + ' ').indexOf('location') > -1)
                && !((' ' + obj.className + ' ').indexOf('openOverlay') > -1)
                && !((' ' + obj.id + ' ').indexOf('action_') > -1))) //Ausnahme LocationList weil das ein Overlay öffnet und nicht gleich schließen soll
            if(obj.instanceOf == null || obj.instanceOf() == this.instanceOf())
            {
                console.log('no Focus on : ' + this.classes[0]);
                closeOverlay();
            }
        };
};