var timer;
var timer_circle;
var circle;
var attackTime = 3000;
var LocationInfo = function (args) {
    Overlay.call(this, args);
    this.locationInfo_id = args['locationInfo_id'];
    this.shown = false;
    this.attackValue = 0;
    this.energy = 0;
    this.canCapture = true;
    this.prevSubTitle = args.prevSubTitle;
    this.shown = function () {
        return this.shown;
    },
    this.show = function () {
        this.locationOwner = current_faction;
        this.map = document.getElementById('map');
        this.container = document.createElement('div');
        this.containerInhalt = document.createElement('div');
        this.containerTitleText = document.createElement('div');
        this.containerTitleText.id = 'LocationName';
        //this.containerTitleText.className = 'TextColorGradient';
        this.containerFactionImage = document.createElement('img');
        this.containerheaderImage = document.createElement('div');
        this.attack = document.createElement('div');
        this.attack_img = document.createElement('img');
        this.nameContainer = document.createElement('div');
        this.info = document.createElement('div');
        this.info_img = document.createElement('img');
        this.attackContainer = document.createElement('div');
        this.attackContainer.id = 'LocationAttackContainer';
        this.attackV = document.createElement('div');
        this.energyV = document.createElement('div');
        this.locationInfo_close = document.createElement('div');
        this.container.id = 'LocationInfo';
        this.containerInhalt.id = 'LocationInfoData';
        this.containerheaderImage.id = 'LocationInfoHeader';
        this.containerFactionImage.id = 'LocationInfoHeaderFactionImage';
        this.container.className = 'animated fadeIn';
        this.attack.id = 'LocationAttackBtn';
        this.attack.className = 'openOverlay';
        this.attack_img.classList.add('openOverlay');
        this.nameContainer.id = 'locatioNameContainer';
        this.nameContainer.className = 'TextColorGradient';
        this.info.id = 'LocationInfoBtn';
        this.info_img.src = './img/buttons/icon_info.png';
        this.attackV.id = 'attackV';
        this.energyV.id = 'energyV';
        this.locationInfo_close.id = 'close';
        this.attackV.innerHTML = '';
        this.energyV.innerHTML = '';

        this.attack.appendChild(this.attack_img);

        this.nameContainer.appendChild(this.containerTitleText);
        this.nameContainer.appendChild(this.energyV);
        this.containerInhalt.appendChild(this.nameContainer);

        this.info.appendChild(this.info_img);
        this.containerInhalt.appendChild(this.info);

        this.attackContainer.appendChild(this.attackV);
        this.attackContainer.appendChild(this.attack);
        this.containerInhalt.appendChild(this.attackContainer);

        this.map.style.backgroundImage = "url('./img/kreuz.png')";

        that = this;

        document.body.appendChild(this.container);
        this.containerheaderImage.appendChild(this.containerFactionImage)
        this.container.appendChild(this.containerheaderImage);
        this.container.appendChild(this.containerInhalt);


        // Loading circle
        circle = new ProgressBar.Circle('#LocationAttackContainer .openOverlay', {
            color: '#FFFFFF',
            duration: 3000,
            strokeWidth: 3,
            easing: 'easeInOut'
        });

        this.load();
        this.shown = true;
        this.info.onclick = function () {
            var args = {};
            args['locationDetail_id'] = that.locationInfo_id;
            args['id'] = 'map';
            args['type'] = 'detail';
            args['prevSubTitle'] = that.prevSubTitle;
            showFragment('Location', args);
        };

        this.refresh();
        this.setBackgroundColorByFaction(this.container);
    },
    this.hide = function () {
    },
    this.close = function () {
      if (timer != null) {
          clearTimeout(timer);
      }
      hud.viewElements.title.setSubTitle(this.prevSubTitle);
      document.body.removeChild(this.container);
      this.shown = false;
    },
    this.isClosed = function () {
    },
    this.isShown = function () {
        return this.shown;
    },
    this.setActionButtons = function () {
        var actions = hud.viewElements['actions'];
        var that = this;
        actions.setActionButtonLeft(ActionButton.EMPTY);
        actions.setActionButtonMiddle(ActionButton.EMPTY);
        actions.setActionButtonRight(new ActionButton(function () {
            that.closeInfo();
        }, './img/buttons/icon_close.png'));
    },
    this.resize = function () {
    },
    this.refresh = function (event) {
        this.toggleAttackBtn();
    },
    this.reload = function (event) {
        this.toggleAttackBtn();
    },
    this.toggleAttackBtn = function() {
        if(!that.canCapture)
            that.attack.style.display = 'none';
        else
            that.attack.style.display = null;

        this.defense = (that.locationOwner!=undefined && that.locationOwner.id == current_faction.id);

        if (checkLocationInRangeWithID(this.locationInfo_id)) { //Ist Location in Range
            if (this.defense) {
                that.attack.id = 'LocationDefenseBtn'; // In Range and Faction owns
                that.attack_img.src = './img/buttons/Defense.png';
            }
            else {
                that.attack.id = 'LocationAttackBtn'; // In Range and owns an other Faction
                that.attack_img.src = './img/buttons/Angriff.png';
            }
            this.attack.onmousedown = function () {
                that.attacke();
            };
        }
        else { //Angriff bzw. Verteidung ausgrauen
            if (this.defense) {
                that.attack.id = 'LocationDefenseBtnGrey'; // In Range and Faction owns
                that.attack_img.src = './img/buttons/Defense_Grau.png';
            }
            else {
                that.attack.id = 'LocationAttackBtnGrey'; // In Range and owns an other Faction
                that.attack_img.src = './img/buttons/Angriff_Grau.png';
            }
            this.attack.onmousedown = function () {};
        }
        if(!this.canCapture)
        {
            that.attackV.style.display = 'none';
            that.attack.style.display = 'none';
        }
    },
    this.load = function () {
        var Loc = Parse.Object.extend("Location");
        var query = new Parse.Query(Loc);
        query.equalTo("objectId", this.locationInfo_id);
        query.include('information');
        query.include('name');
        query.include('headerImage');
        query.include('LocationCaptureData.Faction.logoicon');
        var that = this;
        query.find({
            success: function (results) {
                for (var i = 0; i < results.length; i++) {
                    var object = results[i];
                    var desc = '';
                    that.locationName = '';

                    if(object.get('name')) {
                        if(object.get('name').get('deu')) {
                            that.locationName = object.get('name').get('deu');
                        } else {
                            that.locationName = object.get('name').get('deu');
                        }
                    }

                    if(object.get('headerImage')) {
                        var image = object.get('headerImage').get('image').url();
                        that.containerheaderImage.style.backgroundImage = "url('" + image + "')";
                    }

                    // hud.viewElements.title.setTitle("Location");
                    hud.viewElements.title.setSubTitle(that.locationName);

                    if (!object.get('LocationCaptureData')) {
                        that.canCapture = false;
                    }
                    else {
                        that.energy = object.get('LocationCaptureData').get('energy');
                        that.energyV.innerHTML = that.energy;
                        that.locationOwner = object.get('LocationCaptureData').get('Faction');
                        var factionImage = object.get('LocationCaptureData')
                          .get('Faction')
                          .get('logoicon')
                          .get('icon')
                          .url();
                        that.containerFactionImage.src = factionImage;
                        // .style.backgroundImage = "url('" + factionImage + "')";
                    }
                    that.containerTitleText.innerHTML = that.locationName;

                    that.toggleAttackBtn();
                }
            },
            error: function (error) {
                alert("Error: " + error.code + " " + error.message);
            }
        });
    },

    this.attacke = function () {
      if(currentUser.get("energy") == undefined) {
        $.notify(
          'Ohne Energie kannst du keine Location angreifen. ' +
          'Schließe eine Quest ab, um Energie zu bekommen.',
          'warning');
        return;
      }

        if (timer != null) {
            clearTimeout(timer);
        }

        if (timer_circle != null) {
            circle.stop();
            circle.set(0);
            clearTimeout(timer_circle);
        }

        circle.animate(1);

        timer = setTimeout(this.doAttacke, attackTime);

        // Remove loading circle after full load
        timer_circle = setTimeout(function() {
            circle.set(0);
        }, attackTime);

        this.attackValue += 10;
        if (this.attackValue > currentUser.get("energy")) {
            this.attackValue = 0;
        }
        if(this.defense)
            this.attackV.innerHTML = '+ ' + this.attackValue;
        else
            this.attackV.innerHTML = '- ' + this.attackValue;
        attackValue = this.attackValue;
    },

    this.doAttacke = function () {
        console.log('Angriff mit ' + attackValue);
        if (attackValue == null || attackValue == 0) {
            $.notify('Nicht genug Energy', 'error');
            return;
        }
        params = {
            'location': that.locationInfo_id,
            'spentEnergy': that.attackValue,
            'locale': ''
        };

        that = this;
        Parse.Cloud.run('AttackDefendLocation', params, {
            success: function (result) {
                that.attackValue = 0;
                that.attackV.innerHTML = that.attackValue;
                that.energyV.innerHTML = result.energy;
                updateHud();
                an = new Animation(document.getElementById("energy"));
                an.rubberband();
                an2 = new Animation(document.getElementById("point"));
                an2.rubberband();
                instance.reload('locationAttack');
                $.notify(result.message, 'success');
                closeOverlay('locationInfo'); //Todo just dirty fix, do a clean fix!
            },
            error: function (error) {
                //that.attackV.innerHTML = error.message;
                $.notify(error.message, 'error');
                console.log(error);
            }
        });
    },
    this.closeInfo =function () {
        var args = {};
        args['id'] = 'map';
        showFragment('Map', args);
    }
};
