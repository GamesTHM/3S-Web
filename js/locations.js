/**
 * Created by Yannick on 16.03.2016.
 */


var Locations = function (args) {
    ViewElement.call(this, args);
    Colorable.call(this, args);
    this.addClassName("Locations");


    this.locations = {};
    this.locations['location1'] = new Colorable({'id': 'location1'});
    this.locations['location2'] = new Colorable({'id': 'location2'});
    this.locations['location3'] = new Colorable({'id': 'location3'});
    this.locations['location_button'] = new Colorable({'id': 'location_button'});

    this.setColor = function (color) {

        for (var element in this.locations) {
            if (this.locations[element].instanceOf("Colorable"))
                this.locations[element].setColor(color);
        }
    }
};