var News = function (args) {
  ViewElement.call(this, args);
  this.addClassName("News");

  this.map = document.getElementById('map');
  this.menuButton = document.getElementById("faction");

  this.isShown = function () {
    return this.shown;
  },
  this.show = function (args) {
    disableLocationList();
    hud.viewElements['title'].setTitle("System");
    hud.viewElements['title'].setSubTitle("News");

    this.fragment = document.createElement("div");
    this.fragment.id = "fragment";

    this.fragmentContent = document.createElement("div");
    this.fragmentContent.id = "fragment_content";
    this.fragmentContent.classList.add("faqContainer");

    this.newsTitle = document.createElement("p");
    this.newsTitle.id = "faqTitle";
    this.newsTitle.classList.add("TextColorGradient");
    this.newsTitle.innerHTML = "NEWS";

    this.newsContainer = document.createElement("div");
    this.newsContainer.id = "newsContainer";

    this.userSpeech = document.createElement("p");
    this.userSpeech.id = "userSpeech";

    this.factionContainerTitle = document.createElement("p");
    this.factionContainerTitle.id = "factionContainerTitle";
    this.factionContainerTitle.classList.add("little");
    this.factionContainerTitle.innerHTML = "Aktuelle Fraktionsstatistik";

    this.factionListContainer = document.createElement("div");
    this.factionListContainer.id = "factionListContainer";

    this.newFeaturedQuestDataContainer = document.createElement("div");
    this.newFeaturedQuestDataContainer.id = "newFeaturedQuestDataContainer";

    this.newsContainer.appendChild(this.userSpeech);
    this.newsContainer.appendChild(this.factionContainerTitle);
    this.newsContainer.appendChild(this.factionListContainer);

    this.fragmentContent.appendChild(this.newsTitle);
    this.fragmentContent.appendChild(this.newsContainer);

    this.newsContainer.appendChild(this.newFeaturedQuestDataContainer);

    this.fragment.appendChild(this.fragmentContent);

    this.map.appendChild(this.fragment);

    this.container = this.fragmentContent;
    this.divAktuell = this.fragment;
    this.map.style.backgroundImage = "url('./img/kreuz.png')";
    this.map.className = '';
    this.load();
    this.shown = true;
    this.resize();
    this.fragment.classList.add('content_container');
    this.fragment.classList.add('animated');
    this.fragment.classList.add('slideInUp');
    this.setBackgroundColorByFaction(this.fragment);

  },
  this.close = function (args) {
    enableLocationList();
    this.divAktuell.remove();
    this.shown = false;
  },
  this.load = function () {
    var factionQuery = new Parse.Query("Faction");
    var uQRquery = new Parse.Query("UserQuestRelation");

    this.userSpeech.innerHTML = "Hallo Agent " + hud.viewElements.user.getName().toUpperCase() + "!";

    factionQuery.include("logoicon");
    factionQuery.include("name");
    factionQuery.descending("mainColor");
    factionQuery.find().then(
      this.onFactionsLoaded.bind(this),
      this.onQueryError
    );

    uQRquery.include("user");
    uQRquery.include("quest");
    uQRquery.equalTo("user", currentUser);
    uQRquery.find().then(
      function (results) {
        // this.factionList = [];
        var questArray = [];
        for (var i = 0; i < results.length; i++) {
          var object = results[i];
          questArray[i] = object.get("questID");
        }
        this.findFeaturedQuest(questArray);
      }.bind(this),
      this.onQueryError
    );
    // getWebElementFromCloud('news',this.container);
  },
  this.onFactionsLoaded = function (results) {
    this.factionList = [];
    if (results.length == 0) {
      $.notify("Es wurden keine Fraktionen gefunden","error");
      return;
    }
    for (var i = 0; i < results.length; i++) {
      var object = results[i];
      var id = object.id;
      var factionLogo = object.get('logoicon').get('icon').url();
      var points = object.get('points');
      var name = object.get("name").get('deu');

      this.factionList.push([id, name, points, factionLogo, object]);
    }
    for (i = 0; i < this.factionList.length; i++) {
      var factionObject = this.factionList[i];
      this.printFactionData(factionObject[1], factionObject[2], factionObject[3]);
    }
  },
  this.onQueryError = function (error) {
    $.notify("Error: " + error.code + " " + error.message, "error");
  },
  this.hide = function (args) {
    this.close(args);
  },
  this.setActionButtons = function () {
    actions = hud.viewElements['actions'];
    actions.setActionButtonLeft(
      new ActionButton(
        function () {
          showFragment('News');
        },
        './img/buttons/news/icon_news.png'
      ), true
    );
    actions.setActionButtonMiddle(ActionButton.EMPTY);
    //actions.setActionButtonMiddle(new ActionButton(function () {
    //    showFragment('Faq');
    //}, './img/buttons/news/icon_fractionstory.png'));
    //FAQ ausgeblendet bis wir sie haben.
    actions.setActionButtonRight(ActionButton.CLOSE);
  },
  this.resize = function (event) {
    if (isLandscapeMode()) this.divAktuell.className = 'fragmentLandscape';
    else this.divAktuell.className = ''
  },
  this.printFactionData = function (name, points, logo) {
    // console.log("Faction: " + name + " " + points + " " + logo);

    var faction = document.createElement("div");
    faction.classList.add("newsFaction");
    faction.classList.add("little");

    var factionName = document.createElement("p");
    factionName.classList.add("factionName");
    factionName.innerHTML = name.toUpperCase();

    var factionPoints = document.createElement("p");
    factionPoints.classList.add("factionPoints");
    factionPoints.innerHTML = points;

    var factionImage = document.createElement("img");
    factionImage.classList.add("factionImage");
    factionImage.src = logo;

    var factionImageWrapper = document.createElement("div");
    factionImageWrapper.classList.add("factionImageWrapper");

    faction.appendChild(factionName);
    faction.appendChild(factionPoints);
    factionImageWrapper.appendChild(factionImage);
    faction.appendChild(factionImageWrapper);

    this.factionListContainer.appendChild(faction);
  },
  this.findFeaturedQuest = function (arrayFinishedQuests) {
    var query = new Parse.Query("Quest");

    query.notContainedIn('objectId', arrayFinishedQuests);
    query.equalTo("featured", true);
    query.equalTo("visibility", true);
    query.equalTo("campus", current_campus);
    query.containedIn("faction", [undefined,current_faction]);
    query.lessThanOrEqualTo('startdate', new Date());
    query.greaterThanOrEqualTo("enddate", new Date());
    query.include(["name","shortDescription","description"]);
    query.include(["icon","image"]);
    query.include(["Jobs","Jobs.icon"]);
    query.descending("updatedAt");
    query.limit(1);

    query.find().then(
      this.onFeaturedQuestLoaded.bind(this),
      this.onQueryError
    );
  },
  this.onFeaturedQuestLoaded = function (results) {
    if (results.length == 0) {
      return;
    }

    this.newFeaturedQuestTitle = document.createElement("p");
    this.newFeaturedQuestTitle.id = "newFeaturedQuestTitle";
    this.newFeaturedQuestTitle.classList.add("little");
    this.newFeaturedQuestTitle.innerHTML = "Eine neue Featured Quest ist nun verfügbar";

    this.newFeaturedQuestDataContainer.appendChild(this.newFeaturedQuestTitle);

    var object = results[0];
    var args = {};
    args.icon = '';
    args.image = '';
    args.jobs_array = object.get('Jobs');
    args.id = object.id;
    args.name = object.get('name').get('deu');
    args.desc = object.get('shortDescription').get('deu');

    if (object.get('icon'))
      if (object.get('icon').get('icon'))
        args.icon = object.get('icon').get('icon').url();
    if (object.get('image'))
      if (object.get('image').get('image'))
        args.image = object.get('image').get('image').url();

    // console.log(args.name + " " + args.desc + " " + args.id + " " +
    //   args.image + " " + args.icon + " " + args.jobs_array);

    args.parent_container = this.newFeaturedQuestDataContainer;
    new QuestElement(args);
  }
};
