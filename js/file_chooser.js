/**
 * Basic file chooser
 * Created by yannicklamprecht on 22.08.16.
 */
var FileChooser = function (parentDiv) {
    this.parentDiv=parentDiv;
    this.allowedMimeTypes="image/jpg, image/png, image/jpeg, image/gif, image/bmp, image/tif";
    this.maxAllowedImageUploadSize = 1400000;// ~1.4mb


    var that=this;

    this.fileChooserCallback = function (event) {

        var files = event.target.files;

        if (files.length == 1) {

            console.log(files[0]);

            var file = files[0];
            var name = file.name;
            var fileSize = file.size;

            if(!that.allowedMimeTypes.includes(file.type)){
                $.notify("Your filetype has to be: " + that.allowedMimeTypes.replace(new RegExp("image/",'g'),""), "error");
                return;
            }

            console.log((fileSize>that.maxAllowedImageUploadSize)+" "+ fileSize + " "+that.maxAllowedImageUploadSize);

            if(fileSize == 0 || fileSize > that.maxAllowedImageUploadSize){
                $.notify("Your filesize is to big! Only allowed "+that.displayFileSize(that.maxAllowedImageUploadSize), "error");
                return;
            }


            parseFile = new Parse.File(name, file);
            parseFile.save().then(function () {
                // The file has been saved to Parse.
                currentUser = Parse.User.current();
                currentUser.set('image', parseFile);
                currentUser.save();
                updateHud();

                $.notify("Upload was successful",'success');
            }, function (error) {
                // The file either could not be read, or could not be saved to Parse.
                $.notify("upload error: " + error, "error");
                console.log("upload error: " + error);
            });

        } else {

        }


    }, this.drawUpload = function () {

        this.uploadButtonContainer = document.createElement("input");
        this.uploadButtonContainer.type = "button";
        this.uploadButtonContainer.value = "Bild ändern";
        this.uploadButtonContainer.id="fileChooser";

        this.uploadButtonContainer.onclick = function () {
            document.getElementById('file').click();
        };


        this.fileUpload = document.createElement("input");
        this.fileUpload.type = "file";
        this.fileUpload.id = "file";
        this.fileUpload.setAttribute("accept", this.allowedMimeTypes);

        this.fileUpload.style.display = "none";
        this.fileUpload.addEventListener('change', this.fileChooserCallback, false);


        this.parentDiv.appendChild(this.uploadButtonContainer);
        this.parentDiv.appendChild(this.fileUpload);

    },this.displayFileSize= function(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        if (i == 0) return bytes + ' ' + sizes[i];
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    }


};