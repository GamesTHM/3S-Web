var Quest = function (args) {
    ViewElement.call(this, args);
    this.addClassName("Quest");
    this.menuButton = document.getElementById("quest_btn");

    this.quests = [];
    this.finishedQuests = [];
    this.activeJobOfQuest = {};

    hud.viewElements['title'].setTitle("Quest");
    this.map = document.getElementById('map');
    this.shown = false;

    this.limit = 5;
    this.max = 100;

    this.isShown = function () {
        return this.shown;
    },
    this.show = function () {
      disableLocationList();
      this.map.innerHTML = '<div id="quest_list" class="content_container"></div>';
      this.map.style.backgroundImage = "url('./img/kreuz.png')";
      this.map.className = '';

      this.container = document.getElementById('quest_list');
      this.container.className = 'content_container animated fadeInUp';

      Parse.Promise.when([this.loadJobCheckIcon(),this.loadUserQuestRelations()])
      .then(
        this.load.bind(this,'all'),
        this.onQueryError
      );
      this.shown = true;
    },
    this.close = function () {
      enableLocationList();
      this.container.remove();
      this.shown = false;
    },
    this.load = function (filter) {
      hud.viewElements['title'].setSubTitle(filter);
      if(filter != this.filter) {
        this.filter = filter;
        this.skip =  0;
        this.allQuestsLoaded = false;

        this.container.innerHTML =
          '<div id="loadBtnDiv">' +
            '<label id="loadingLabel" class="centered-lbl">Quests werden geladen...</label>' +
          '</div>';
        document.getElementById('quest_list').onscroll = function(e) {
          if(
            e.target.scrollTop >=
            (e.target.scrollHeight - e.target.offsetHeight - 100)
            && !this.loading
            && !this.allQuestsLoaded
          ) {
            this.loading = true;
            this.load(filter);
          }
        }.bind(this);

        this.label = document.getElementById('loadingLabel');
      } else {
        this.label.style.display = "block";
        this.label.innerHTML = "Lädt...";
      }

      var uqrQuery = new Parse.Query("UserQuestRelation");
      uqrQuery.equalTo("user",currentUser)
      .doesNotExist("activejob");

      var query = new Parse.Query("Quest");
      if (filter == 'featured')
          query.equalTo('featured', true);
      if (filter == 'daily')
          query.greaterThanOrEqualTo('timeToLive', 1);
      query.include(['name','shortDescription'])
      .include(['image','icon'])
      .include(['Jobs','Jobs.icon'])
      .lessThanOrEqualTo('startdate', new Date())
      .greaterThanOrEqualTo("enddate", new Date())
      .equalTo("campus", current_campus)
      .equalTo("visibility", true)
      .containedIn("faction", [undefined,current_faction])
      .doesNotMatchKeyInQuery("objectId","questID",uqrQuery) //exclude finished quests
      .descending("featured")
      .limit(this.limit)
      .skip(this.skip)
      .find().then(
        this.onQuestsLoaded.bind(this),
        this.onQueryError
      );

      this.skip += this.limit;
    },
    this.onQuestsLoaded = function (results) {
      if(this.label.style.display != "none") {
        this.label.style.display = "none";
        if(
          results.length == 0
          && this.label.innerHTML != "Lädt..."
        ) {
          console.log('Keine Quests vorhanden!');
          $.notify("Keine Quests vorhanden!", "warning");
        }
      }

      if(this.limit > results.length) {
        this.allQuestsLoaded = true;
      }

      for (var i = 0; i < results.length; i++) {
        var object = results[i];
        var args = {};
        args.icon = '';
        args.image = '';
        args.jobs_array = object.get('Jobs');
        args.id = object.id;
        args.name = object.get('name').get('deu');
        args.desc = object.get('shortDescription').get('deu');
        if (object.get('icon'))
            if (object.get('icon').get('icon'))
                args.icon = object.get('icon').get('icon').url();
        if (object.get('image'))
            if (object.get('image').get('image'))
                args.image = object.get('image').get('image').url();
        var user = Parse.User.current();

        if(this.activeJobOfQuest[args.id]) {
          console.log('Quest ist aktiv');
          args.activeJobId = this.activeJobOfQuest[args.id];
          args.jobCheckIconUrl = this.jobCheckIconUrl;
          this.create(args);
        } else {
          console.log('neuer Quest');
          this.create(args);
        }
      }
      this.loading = false;
    },
    this.create = function (args) {
      args.parent_container = document.getElementById('quest_list');
      new QuestElement(args);
    },
    this.loadJobCheckIcon = function() {
      var query = new Parse.Query("GUIGraphic");

      query.equalTo("graphicName","job-haken");

      return query.first().then(
        this.onJobIconLoaded.bind(this),
        this.onQueryError
      );
    },
    this.onJobIconLoaded = function(jobIcon) {
      this.jobCheckIconUrl = jobIcon.get("graphic").url();
    },
    this.loadUserQuestRelations = function() {
      var uqrQuery = new Parse.Query("UserQuestRelation");

      uqrQuery.equalTo("user",Parse.User.current())
      .exists("activejob");

      return uqrQuery.find().then(
        this.onUqrQuerySuccess.bind(this),
        this.onQueryError
      );
    },
    this.onUqrQuerySuccess = function(uqrs) {
      for (var i = 0; i < uqrs.length; i++) {
        this.activeJobOfQuest[uqrs[i].get("questID")] = uqrs[i].get("activejob").id;
      }
    },
    this.onQueryError = function(error) {
      $.notify("Fehler beim Laden der Questliste", "error");
    },
    this.setActionButtons = function () {
      hud.viewElements['title'].setSubTitle('all');
      hud.viewElements['actions'].setActionButtonLeft(
        new ActionButton(
          function () {
              this.load('featured');
          }.bind(this),
          './img/buttons/quest/icon_featuredquest.png'
        )
      );
      hud.viewElements['actions'].setActionButtonMiddle(
        new ActionButton(
          function () {
            this.load('all');
          }.bind(this),
          './img/buttons/quest/icon_nearquest.png'
        ),
        true //set this as the active action button
      );
      hud.viewElements['actions'].setActionButtonRight(
        new ActionButton(
          function () {
            this.load('daily');
          }.bind(this),
          './img/buttons/quest/icon_dailyquest.png'
        )
      );
    }
};
