# Recommendations
------
**CleanUp**
1. take a step further

+ modularize hud
    + ranklistView
    + right faction/group view
    + left toggles map/quest/toplistStats -> objectgroup
    + reportFlag left-bottom-corner
    + userAccountSettings
    + group of username/statView/energyView
    + studyEnergyImproveButton

**Contribution Guide definition (will be added as contributionGuide if its finished)**

+ each feature/improvement into branch until it's finished and tested
+ short description for each file to explain new functions or changes

------













------
**Ideas for next steps (Framework)**

+ Map with locations -> partly connected with each other
    + On the way from one to another location you can
        + pickup certain items for quest solving
            + items have certain chances to appear
    + lecturer appear randomly to give you entryQuests than they retreat to their specific location
        + example location electrotechnic laboratory
+ EntryQuests are 
    + most times collectionQuests
    + have min-point barrier
+ To move between locations you only have to click the targetLocation
    + the char automatically moves towards this location
    + hopefully shortest path first
+ special mode
    + "Bafög"-mode
        + you've a certain startAmount of collectionItems
        + actions has to be considered wise
            + each part of the way is count
            + if a amount of quests isn't absolved in certain wayCount, 
                1. "Bafög" will be canceled -> the startItemContingent is set to zero
                2. after absolving every quest you've to pay back your "Bafög"
                3. you get an negative amount in height of the startAmount
                4. you've to walk through your university to collect the items to get to zero
                5. (motivates the user to play the game longer -> time to get new quests implemented)
    + "Normal"-mode
        + takes more time
        + don't know features
                    
                    